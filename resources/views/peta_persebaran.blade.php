@extends('layouts.base')

@section('title', 'Peta Persebaran Kasus')
@section('meta-description', 'Halaman peta sebaran kasus portal web resmi Pusat Informasi COVID-19 Kota Bandung. Detail mengenai persebaran kasus COVID-19 per kecamatan hingga per kelurahan. Copyright © 2022 Pemerintah Kota Bandung.')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.5.css') }}">
@endsection

@section('main-content')
    <!--================Breadcrumb Area =================-->
    <section class="breadcrumb_area">
        <div class="container">
            <div class="breadcrumb_text text-left">
                <h3 class="animate">Peta Persebaran COVID-19</h3>
                <h6 class="animate">
                    {{ $kecamatan === null ? 'Kota Bandung' : "Kecamatan $kecamatan" }}
                </h6>
                <ul class="nav">
                    <li class="animate"><a href="{{ url('') }}">Beranda</a></li>
                    @if ($kecamatan !== null)
                        <li class="animate"><a href="{{ url('/peta-persebaran') }}">Peta Persebaran</a></li>
                        <li class="animate">Kecamatan {{ $kecamatan }}</li>
                    @else
                        <li class="animate">Peta Persebaran</li>
                    @endif
                </ul>
            </div>
        </div>
    </section>
    <!--================End Breadcrumb Area =================-->

    <!--================Worldwide Tracker Area =================-->
    <section class="world_wide_tracker pt-3" style="background-color: rgba(0, 0, 0, .05)">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 mt-3">
                    <div class="card rounded-card left-border animate">
                        <div class="card-body text-center">
                            <div class="d-flex flex-row justify-content-center">
                                @if ($kecamatan !== null)
                                    <a href="{{ url('peta-persebaran') }}" class="btn btn-sm btn-outline-info" style="position: absolute; left: 18px">
                                        <i data-feather="arrow-left"></i>
                                    </a>
                                @endif
                                <h5 class="px-5">
                                    Persebaran COVID-19 di
                                    {{ $kecamatan === null ? 'Kota Bandung' : "Kecamatan $kecamatan" }}
                                </h5>
                            </div>
                            <p>
                                Data per tanggal: {{ $latest_date }}<br>
                                <u>https://covid19.bandung.go.id</u>
                            </p>
                            <div id="map"></div>
                            @if ($kecamatan === null)
                                <small class="text-left">Klik salah satu kecamatan untuk melihat detail kasus per kecamatan</small>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 mt-3">
                    <div class="card rounded-card left-border animate">
                        <div class="card-body text-center pb-5" style="height: {{ $kecamatan === null ? '720px' : '360px' }}">
                            <h5>Kasus Aktif</h5>
                            <canvas id="cases"></canvas>
                        </div>
                    </div>
                </div><div class="col-12 col-lg-4 mt-3">
                    <div class="card case-counter animate rounded-card left-border">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center px-2">
                                <h2 id="total-active">
                                    {{ number_format($latest_updates['terkonfirmasi_aktif']) }}
                                    <span class="text-danger ml-2">
                                        <i data-feather="{{ $latest_updates['terkonfirmasi_aktif'] - $previous_updates['terkonfirmasi_aktif'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n1"></i>
                                        {{ number_format(abs($latest_updates['terkonfirmasi_aktif'] - $previous_updates['terkonfirmasi_aktif'])) }}
                                    </span>
                                </h2>
                                <p>Konfirmasi Aktif</p>
                            </div>
                            <div class="absolute">
                                <img src="{{ asset('assets/images/chevrons-up.svg') }}" width="200px" height="200px" class="no-rotate" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 mt-3">
                    <div class="card case-counter animate rounded-card left-border">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center px-2">
                                <h2 id="total-active">
                                    {{ number_format($latest_updates['terkonfirmasi_sembuh']) }}
                                    <span class="text-danger ml-2">
                                        <i data-feather="{{ $latest_updates['terkonfirmasi_sembuh'] - $previous_updates['terkonfirmasi_sembuh'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n1"></i>
                                        {{ number_format(abs($latest_updates['terkonfirmasi_sembuh'] - $previous_updates['terkonfirmasi_sembuh'])) }}
                                    </span>
                                </h2>
                                <p>Konfirmasi Sembuh</p>
                            </div>
                            <div class="absolute">
                                <img src="{{ asset('assets/images/chevrons-up.svg') }}" width="200px" height="200px" class="no-rotate" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 mt-3">
                    <div class="card case-counter animate rounded-card left-border">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center px-2">
                                <h2 id="total-active">
                                    {{ number_format($latest_updates['terkonfirmasi_meninggal']) }}
                                    <span class="text-danger ml-2">
                                        <i data-feather="{{ $latest_updates['terkonfirmasi_meninggal'] - $previous_updates['terkonfirmasi_meninggal'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n1"></i>
                                        {{ number_format(abs($latest_updates['terkonfirmasi_meninggal'] - $previous_updates['terkonfirmasi_meninggal'])) }}
                                    </span>
                                </h2>
                                <p>Konfirmasi Meninggal</p>
                            </div>
                            <div class="absolute">
                                <img src="{{ asset('assets/images/chevrons-up.svg') }}" width="200px" height="200px" class="no-rotate" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 mt-3 mt-lg-4">
                    <div class="card case-counter animate rounded-card left-border">
                        <div class="card-body">
                            <h3 class="text-center">Suspek</h3>
                            <div class="row mt-4 text-center">
                                <div class="col-4">
                                    <h3>{{ number_format($latest_updates['total_suspek']) }}</h3>
                                    <p class="text-danger mt-n2">
                                        <i data-feather="{{ $latest_updates['total_suspek'] - $previous_updates['total_suspek'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                        {{ number_format(abs($latest_updates['total_suspek'] - $previous_updates['total_suspek'])) }}
                                    </p>
                                    <p>Total</p>
                                </div>
                                <div class="col-4">
                                    <h3>{{ number_format($latest_updates['suspek_dalam_pantauan']) }}</h3>
                                    <p class="text-danger mt-n2">
                                        <i data-feather="{{ $latest_updates['suspek_dalam_pantauan'] - $previous_updates['suspek_dalam_pantauan'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                        {{ number_format(abs($latest_updates['suspek_dalam_pantauan'] - $previous_updates['suspek_dalam_pantauan'])) }}
                                    </p>
                                    <p>Dipantau</p>
                                </div>
                                <div class="col-4">
                                    <h3>{{ number_format($latest_updates['suspek_discarded']) }}</h3>
                                    <p class="text-danger mt-n2">
                                        <i data-feather="{{ $latest_updates['suspek_discarded'] - $previous_updates['suspek_discarded'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                        {{ number_format(abs($latest_updates['suspek_discarded'] - $previous_updates['suspek_discarded'])) }}
                                    </p>
                                    <p>Discarded</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 mt-3 mt-lg-4">
                    <div class="card case-counter animate rounded-card left-border">
                        <div class="card-body">
                            <h3 class="text-center">Kontak Erat</h3>
                            <div class="row mt-4 text-center">
                                <div class="col-4">
                                    <h3>{{ number_format($latest_updates['total_kontak_erat']) }}</h3>
                                    <p class="text-danger mt-n2">
                                        <i data-feather="{{ $latest_updates['total_kontak_erat'] - $previous_updates['total_kontak_erat'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                        {{ number_format(abs($latest_updates['total_kontak_erat'] - $previous_updates['total_kontak_erat'])) }}
                                    </p>
                                    <p>Total</p>
                                </div>
                                <div class="col-4">
                                    <h3>{{ number_format($latest_updates['kontak_erat_dalam_pantauan']) }}</h3>
                                    <p class="text-danger mt-n2">
                                        <i data-feather="{{ $latest_updates['kontak_erat_dalam_pantauan'] - $previous_updates['kontak_erat_dalam_pantauan'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                        {{ number_format(abs($latest_updates['kontak_erat_dalam_pantauan'] - $previous_updates['kontak_erat_dalam_pantauan'])) }}
                                    </p>
                                    <p>Dipantau</p>
                                </div>
                                <div class="col-4">
                                    <h3>{{ number_format($latest_updates['kontak_erat_discarded']) }}</h3>
                                    <p class="text-danger mt-n2">
                                        <i data-feather="{{ $latest_updates['kontak_erat_discarded'] - $previous_updates['kontak_erat_discarded'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                        {{ number_format(abs($latest_updates['kontak_erat_discarded'] - $previous_updates['kontak_erat_discarded'])) }}
                                    </p>
                                    <p>Discarded</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Worldwide Tracker Area =================-->
@endsection

@section('custom-js')
<script src="{{ asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.5.min.js') }}"></script>
<script src="{{ asset(
    $kecamatan === null ? 'assets/js/json-map/bandung.js' : 'assets/js/json-map/' . strtolower(str_replace(' ', '-', $kecamatan)) . '.js'
) }}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
<script src="{{ asset('assets/js/peta-persebaran.js') }}"></script>
@endsection