@extends('layouts.dashboard')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex flex-row justify-content-between align-items-center flex-wrap">
                        <div>
                            Data diperbarui pada tanggal
                            <h4>{{ $latest_updates['date'] }}</h4>
                        </div>
                        <div class="d-flex flex-row flex-wrap">
                            <div class="dropdown me-2 mt-2 mt-lg-0">
                                <button class="btn btn-outline-success dropdown-toggle" data-bs-toggle="dropdown">Perbarui Data Vaksin</button>
                                <div class="dropdown-menu">
                                    <form action="{{ url('fetch/vaksin-splp') }}" method="POST" onsubmit="return confirm('Perbarui data vaksinasi dari SPLP?')">
                                        @csrf
                                        <button class="dropdown-item" value="vaksin">Dari SPLP</button>
                                    </form>
                                    <form action="{{ url('fetch/vaksin-mantra') }}" method="POST" onsubmit="return confirm('Perbarui data vaksinasi dari mantra.bandung.go.id?')">
                                        @csrf
                                        <button class="dropdown-item" value="vaksin">Dari mantra.bandung.go.id</button>
                                    </form>
                                    <form action="{{ url('fetch/vaksin-data-bandung') }}" method="post" onsubmit="return confirm('Perbarui data vaksinasi dari data.bandung.go.id?')">
                                        @csrf
                                        <button class="dropdown-item" value="mantra">Dari data.bandung.go.id</button>
                                    </form>
                                    {{-- <button class="dropdown-item" data-bs-toggle="modal" data-bs-target="#excel-form-vaccine">Import excel</button> --}}
                                </div>
                            </div>
                            <div class="dropdown mt-2 mt-lg-0">
                                <button class="btn btn-outline-success dropdown-toggle" data-bs-toggle="dropdown">Perbarui Data Kasus</button>
                                <div class="dropdown-menu">
                                    <form action="{{ url('fetch/splp') }}" method="post" onsubmit="return confirm('Perbarui data dari SPLP?')">
                                        @csrf
                                        <button class="dropdown-item" value="mantra">Dari SPLP</button>
                                    </form>
                                    <form action="{{ url('fetch/mantra') }}" method="post" onsubmit="return confirm('Perbarui data dari mantra.bandung.go.id?')">
                                        @csrf
                                        <button class="dropdown-item" value="mantra">Dari mantra.bandung.go.id</button>
                                    </form>
                                    <form action="{{ url('fetch/data-bandung') }}" method="post" onsubmit="return confirm('Perbarui data dari mantra.bandung.go.id?')">
                                        @csrf
                                        <button class="dropdown-item" value="mantra">Dari data.bandung.go.id</button>
                                    </form>
                                    <button class="dropdown-item" data-bs-toggle="modal" data-bs-target="#excel-form">Import excel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (session('error') || session('success'))
                        <div class="alert {{ session('error') ? 'alert-danger' : 'alert-success' }} mt-4">
                            {{ session('error') ?: session('success') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body d-flex align-items-center justify-content-between">
                    <div>
                        <h1>{{ number_format($latest_updates['total_terkonfirmasi']) }}</h1>
                        <span class="text-muted">Total Terkonfirmasi</span>
                    </div>
                    <h3 class="text-warning">
                        <i class="icon-chevrons-{{ $latest_updates['total_terkonfirmasi'] - $previous_updates['total_terkonfirmasi'] < 0 ? 'down' : 'up' }} feather"></i>
                        {{ number_format(abs($latest_updates['total_terkonfirmasi'] - $previous_updates['total_terkonfirmasi'])) }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body d-flex align-items-center justify-content-between">
                    <div>
                        <h1>{{ number_format($latest_updates['terkonfirmasi_aktif']) }}</h1>
                        <span class="text-muted">Terkonfirmasi Aktif</span>
                    </div>
                    <h3 class="text-danger">
                        <i class="icon-chevrons-{{ $latest_updates['terkonfirmasi_aktif'] - $previous_updates['terkonfirmasi_aktif'] < 0 ? 'down' : 'up' }} feather"></i>
                        {{ number_format(abs($latest_updates['terkonfirmasi_aktif'] - $previous_updates['terkonfirmasi_aktif'])) }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body d-flex align-items-center justify-content-between">
                    <div>
                        <h1>{{ number_format($latest_updates['terkonfirmasi_sembuh']) }}</h1>
                        <span class="text-muted">Terkonfirmasi Sembuh</span>
                    </div>
                    <h3 class="text-success">
                        <i class="icon-chevrons-{{ $latest_updates['terkonfirmasi_sembuh'] - $previous_updates['terkonfirmasi_sembuh'] < 0 ? 'down' : 'up' }} feather"></i>
                        {{ number_format(abs($latest_updates['terkonfirmasi_sembuh'] - $previous_updates['terkonfirmasi_sembuh'])) }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body d-flex align-items-center justify-content-between">
                    <div>
                        <h1>{{ number_format($latest_updates['terkonfirmasi_meninggal']) }}</h1>
                        <span class="text-muted">Terkonfirmasi Meninggal</span>
                    </div>
                    <h3 class="text-black">
                        <i class="icon-chevrons-{{ $latest_updates['terkonfirmasi_meninggal'] - $previous_updates['terkonfirmasi_meninggal'] < 0 ? 'down' : 'up' }} feather"></i>
                        {{ number_format(abs($latest_updates['terkonfirmasi_meninggal'] - $previous_updates['terkonfirmasi_meninggal'])) }}
                    </h3>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="excel-form">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Perbarui Data Kasus dari Excel</h5>
                    <button class="close" data-bs-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('fetch/excel') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label class="form-label">File Excel (.xlsx)</label>
                            <input type="file" class="form-control" name="file" placeholder="Template Pusicov 220220220000">
                        </div>
                        <button class="btn btn-outline-success float-end">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="excel-form-vaccine">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Perbarui Data Vaksinasi dari Excel</h5>
                    <button class="close" data-bs-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('fetch/excel-vaksin') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label class="form-label">File Excel (.xlsx)</label>
                            <input type="file" class="form-control" name="file" placeholder="Template Vaksinasi 220220220000">
                        </div>
                        <button class="btn btn-outline-success float-end">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection