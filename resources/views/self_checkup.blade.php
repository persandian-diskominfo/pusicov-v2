@extends('layouts.base')

@section('title', 'Cek Mandiri')
@section('name', 'Halaman cek mandiri portal web resmi Pusat Informasi COVID-19 Kota Bandung. Anda dapat melakukan cek mandiri terkait gejala yang Anda rasakan apakah memiliki kaitan dengan COVID-19. Copyright © 2022 Pemerintah Kota Bandung.')

@section('main-content')
    <!--================Breadcrumb Area =================-->
    <section class="breadcrumb_area">
        <div class="container">
            <div class="breadcrumb_text text-left">
                <h3 class="animate">Cek Mandiri</h3>
                <h6 class="animate">COVID-19</h6>
                <ul class="nav">
                    <li class="animate"><a href="{{ url('') }}">Beranda</a></li>
                    <li class="animate">Cek Mandiri</li>
                </ul>
            </div>
        </div>
    </section>
    <!--================End Breadcrumb Area =================-->

    <section>
        <div class="pagepiling">
            <form id="self-check-form">
                <div class="pp-scrollable section active">
                    <div class="scroll-wrap">
                        <div class="scrollable-content">
                            <div class="container">
                                <div class="virus_checker_content">
                                    <div class="checked_button_info">
                                        <h4 class="animate">Apakah Anda mengalami salah satu dari gejala berikut?</h4>
                                        <ol type="1" class="mt-n3">
                                            <li class="animate">Kesulitan bernafas yang parah (bernafas dengan sangat cepat atau bicara dalam satu kata)</li>
                                            <li class="animate">Nyeri dada yang parah</li>
                                            <li class="animate">Sulit untuk bangun</li>
                                            <li class="animate">Merasa kebingungan</li>
                                            <li class="animate">Penurunan kesadaran</li>
                                        </ol>
                                        <div class="mt-5">
                                            <div class="check_btn moveDown animate">
                                                <input type="radio" name="symptom1" value="urgent" />
                                                <label for="ya">Ya</label>
                                            </div>
                                            <div class="check_btn moveDown animate">
                                                <input type="radio" name="symptom1" value="tidak" />
                                                <label for="tidak">Tidak</label>
                                            </div>
                                            {{-- <a class="btn btn-success" data-toggle="modal" data-target="#probable-modal">Probable</a>
                                            <a class="btn btn-success" data-toggle="modal" data-target="#improbable-modal">Improbable</a>
                                            <a class="btn btn-success" data-toggle="modal" data-target="#urgent-modal">Urgent</a> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
                <div class="pp-scrollable section">
                    <div class="scroll-wrap">
                        <div class="scrollable-content">
                            <div class="container">
                                <div class="virus_checker_content">
                                    <div class="checked_button_info">
                                        <h4>Apakah Anda mengalami salah satu dari gejala berikut?</h4>
                                        <ol type="1" class="mt-n3">
                                            <li>Nafas yang pendek saat istirahat</li>
                                            <li>Ketidakmampuan untuk berbaring karena kesulitan bernafas</li>
                                            <li>Kondisi kesehatan kronis yang Anda alami dirasakan lebih berat karena kesulitan bernafas</li>
                                        </ol>
                                        <div class="mt-5">
                                            <div class="check_btn moveDown">
                                                <input type="radio" name="symptom2" value="urgent" />
                                                <label for="ya">Ya</label>
                                            </div>
                                            <div class="check_btn moveDown">
                                                <input type="radio" name="symptom2" value="tidak" />
                                                <label for="tidak">Tidak</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pp-scrollable section">
                    <div class="scroll-wrap">
                        <div class="scrollable-content">
                            <div class="container">
                                <div class="virus_checker_content">
                                    <div class="checked_button_info">
                                        <h4>Apakah Anda mengalami salah satu dari gejala berikut?</h4>
                                        <ol type="1" class="mt-n3">
                                            <li>Demam</li>
                                            <li>Batuk</li>
                                            <li>Bersin</li>
                                            <li>Sakit tenggorokan</li>
                                            <li>Sulit bernafas</li>
                                        </ol>
                                        <div class="mt-5">
                                            <div class="check_btn moveDown">
                                                <input type="radio" name="symptom3" value="improbable" />
                                                <label for="ya">Ya</label>
                                            </div>
                                            <div class="check_btn moveDown">
                                                <input type="radio" name="symptom3" value="tidak" />
                                                <label for="tidak">Tidak</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pp-scrollable section">
                    <div class="scroll-wrap">
                        <div class="scrollable-content">
                            <div class="container">
                                <div class="virus_checker_content">
                                    <div class="checked_button_info">
                                        <h4>Apakah Anda pernah merasakan ada gejala sekitar 14 hari setelah berkunjung ke luar negeri (Tiongkok, Italia, Iran, Korea Selatan, Prancis, Spanyol, Jerman, atau Amerika Serikat) atau ke kota terjangkit (Jakarta, Bali, Solo, Yogyakarta, Pontianak, Manado, dll)?</h4>
                                        <div class="mt-5">
                                            <div class="check_btn moveDown">
                                                <input type="radio" name="symptom4" value="probable" />
                                                <label for="ya">Ya</label>
                                            </div>
                                            <div class="check_btn moveDown">
                                                <input type="radio" name="symptom4" value="tidak" />
                                                <label for="tidak">Tidak</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pp-scrollable section">
                    <div class="scroll-wrap">
                        <div class="scrollable-content">
                            <div class="container">
                                <div class="virus_checker_content">
                                    <div class="checked_button_info">
                                        <h4>Apakah Anda pernah atau sedang memberikan perawatan atau melakukan kontak erat dengan seseorang yang (kemungkinan atau dikonfirmasi) terinfeksi COVID-19 saat mereka sakit (batuk, demam, bersin, atau sakit tenggorokan)?</h4>
                                        <div class="mt-5">
                                            <div class="check_btn moveDown">
                                                <input type="radio" name="symptom5" value="probable" />
                                                <label for="ya">Ya</label>
                                            </div>
                                            <div class="check_btn moveDown">
                                                <input type="radio" name="symptom5" value="tidak" />
                                                <label for="tidak">Tidak</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="pp-scrollable section">
                    <div class="scroll-wrap">
                        <div class="scrollable-content">
                            <div class="container">
                                <div class="virus_checker_content">
                                    <div class="checked_button_info">
                                        <h4>Apakah Anda memiliki kontak dekat dengan seseorang yang bepergian ke luar negeri dalam 14 hari terakhir yang menjadi sakit (batuk, demam, bersin, atau sakit tenggorokan)?</h4>
                                        <div class="mt-5">
                                            <div class="check_btn moveDown last">
                                                <input type="radio" name="symptom6" value="probable" />
                                                <label for="ya">Ya</label>
                                            </div>
                                            <div class="check_btn moveDown last">
                                                <input type="radio" name="symptom6" value="tidak" />
                                                <label for="tidak">Tidak</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pp-scrollable section">
                    <div class="scroll-wrap">
                        <div class="scrollable-content">
                            <div class="container">
                                <div class="virus_checker_content">
                                    <div class="checked_button_info">
                                        <h4>Terima kasih telah menggunakan fitur cek mandiri COVID-19 PUSICOV Kota Bandung.</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('modal-section')
<div class="modal fade recommenddModal" id="urgent-modal" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="scroll_body">
                <div class="main_title">
                    <h3>Rekomendasi Berdasarkan Kondisi Anda</h3>
                </div>
                <h6 class="text-danger mt-4" style="line-height: 22pt">Gejala-gejala ini membutuhkan perhatian segera. Anda harus segera menelepon rumah sakit terdekat, atau langsung mendatangi instalasi gawat darurat terdekat.</h6>
            </div>
            <div class="d-flex flex-row">
                <a href="{{ url()->current() }}" class="exit_btn mt-1">Keluar</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade recommenddModal" id="probable-modal" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="scroll_body">
                <div class="main_title">
                    <h3 class="text-center">Rekomendasi Berdasarkan Kondisi Anda</h3>
                </div>
                <div class="text-danger mt-4" style="line-height: 22pt; font-size: 14pt; text-indent: 32pt; text-align: justify">
                    <p>
                        Sebagai tindakan pencegahan, kami merekomendasikan siapa saja yang mengalami gejala (demam, batuk, bersin, sakit tenggorokan, atau sulit bernafas) untuk tinggal di rumah selama 14 hari untuk mencegah penyebaran.
                        Anda disarankan untuk tidak keluar rumah mengunjungi tempat publik, dan tidak menerima tamu di rumah.
                    </p>
                    <p>
                        Gejala Anda ada kemungkinan untuk dapat sembuh sendiri dengan makan dan istirahat yang cukup, namun Anda disarankan untuk menelepon Call Center COVID-19 Kota Bandung di <a href="tel:119">119</a> atau di <a href="tel:112">112</a>.
                        Anda dimohon untuk tidak mendatangi UGD, rumah sakit, atau klinik, kecuali jika gejala yang Anda alami semakin memburuk silakan kunjungi dokter terdekat.
                    </p>
                </div>
            </div>
            <div class="d-flex flex-row">
                <a href="{{ url()->current() }}" class="exit_btn mt-1">Keluar</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade recommenddModal" id="improbable-modal" data-backdrop="static" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="scroll_body">
                <div class="main_title">
                    <h3 class="text-center">Rekomendasi Berdasarkan Kondisi Anda</h3>
                </div>
                <div class="text-success mt-4" style="line-height: 22pt; font-size: 14pt; text-indent: 32pt; text-align: justify">
                    <p>
                        Anda kemungkinan besar tidak terinfeksi COVID-19, namun Anda disarankan untuk tetap tinggal di rumah. Apabila ada gejala yang Anda rasakan mungkin bukan disebabkan oleh virus COVID-19, oleh karena itu Anda tidak perlu melakukan tes COVID-19.
                        Meskipun demikian, hindari keluar rumah jika memungkinkan, hindari kerumunan, tetap memakai masker, dan menjaga jarak. Apabila ada gejala yang Anda rasakan kemungkinan akan sembuh dengan makan dan istirahat yang cukup.
                    </p>
                    <p>
                        Apabila Anda mengalami gejala atau mendapatkan informasi baru tentang penyakit yang Anda rasakan, Anda dapat melakukan cek mandiri lagi pada laman ini.
                    </p>
                </div>
            </div>
            <div class="d-flex flex-row">
                <a href="{{ url()->current() }}" class="exit_btn mt-1">Keluar</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')
    <script src="{{ asset('assets/js/self-check.js') }}"></script>
@endsection