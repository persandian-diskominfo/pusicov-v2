@extends('layouts.base')

@section('title', 'Data Vaksinasi')
@section('meta-description', 'Halaman detail vaksinasi portal web resmi Pusat Informasi COVID-19 Kota Bandung. Lihat data mengenai sasaran vaksinasi, dan ketercapaian dosis vaksin pertama, kedua, dan ketiga. Copyright © 2022 Pemerintah Kota Bandung.')

@section('main-content')
    <!--================Breadcrumb Area =================-->
    <section class="breadcrumb_area">
        <div class="container">
            <div class="breadcrumb_text text-left">
                <h3 class="animate">Data Vaksinasi COVID-19</h3>
                <h6 class="animate">Kota Bandung</h6>
                <ul class="nav">
                    <li class="animate"><a href="{{ url('') }}">Beranda</a></li>
                    <li class="animate">Data Vaksinasi</li>
                </ul>
            </div>
        </div>
    </section>
    <!--================End Breadcrumb Area =================-->

    <!--================Worldwide Tracker Area =================-->
    <section class="world_wide_tracker pt-5" style="background-color: rgba(0, 0, 0, .05)">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card rounded-card left-border animate mt-3">
                        <div class="card-body text-center">
                            <h4>Sasaran Vaksinasi</h4>
                            <p class="mt-n2">Data per tanggal {{ $date }}</p>
                            <div class="row mt-5 text-center vaccine-data">
                                <div class="col-6 col-lg-2">
                                    <h3>{{ number_format($vaksin['sasaran_nakes']) }}</h3>
                                    <p>Tenaga Kesehatan</p>
                                </div>
                                <div class="col-6 col-lg-2">
                                    <h3>{{ number_format($vaksin['sasaran_petugas_publik']) }}</h3>
                                    <p>Petugas Publik</p>
                                </div>
                                <div class="col-6 col-lg-2">
                                    <h3>{{ number_format($vaksin['sasaran_lansia']) }}</h3>
                                    <p>Lansia</p>
                                </div>
                                <div class="col-6 col-lg-2">
                                    <h3>{{ number_format($vaksin['sasaran_masyarakat']) }}</h3>
                                    <p>Masyarakat</p>
                                </div>
                                <div class="col-6 col-lg-2">
                                    <h3>{{ number_format($vaksin['sasaran_remaja']) }}</h3>
                                    <p>Remaja</p>
                                </div>
                                <div class="col-6 col-lg-2">
                                    <h3>{{ number_format($vaksin['sasaran_anak']) }}</h3>
                                    <p>Anak-anak</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card rounded-card left-border animate mt-3">
                        <div class="card-body text-center">
                            <h5>Ketercapaian Vaksin Dosis 1</h5>
                            <div class="row mt-5 text-center vaccine-data">
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_1']['Tenaga Kesehatan'] / $vaksin['sasaran_nakes'] * 100, 2) }}%</h3>
                                    <p>Tenaga Kesehatan</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_1']['Petugas Publik'] / $vaksin['sasaran_petugas_publik'] * 100, 2) }}%</h3>
                                    <p>Petugas Publik</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_1']['Lansia'] / $vaksin['sasaran_lansia'] * 100, 2) }}%</h3>
                                    <p>Lansia</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_1']['Masyarakat'] / $vaksin['sasaran_masyarakat'] * 100, 2) }}%</h3>
                                    <p>Masyarakat</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_1']['Remaja'] / $vaksin['sasaran_remaja'] * 100, 2) }}%</h3>
                                    <p>Remaja</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_1']['Anak'] / $vaksin['sasaran_anak'] * 100, 2) }}%</h3>
                                    <p>Anak-anak</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card rounded-card left-border animate mt-3">
                        <div class="card-body text-center">
                            <h5>Ketercapaian Vaksin Dosis 2</h5>
                            <div class="row mt-5 text-center vaccine-data">
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_2']['Tenaga Kesehatan'] / $vaksin['sasaran_nakes'] * 100, 2) }}%</h3>
                                    <p>Tenaga Kesehatan</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_2']['Petugas Publik'] / $vaksin['sasaran_petugas_publik'] * 100, 2) }}%</h3>
                                    <p>Petugas Publik</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_2']['Lansia'] / $vaksin['sasaran_lansia'] * 100, 2) }}%</h3>
                                    <p>Lansia</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_2']['Masyarakat'] / $vaksin['sasaran_masyarakat'] * 100, 2) }}%</h3>
                                    <p>Masyarakat</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_2']['Remaja'] / $vaksin['sasaran_remaja'] * 100, 2) }}%</h3>
                                    <p>Remaja</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_2']['Anak'] / $vaksin['sasaran_anak'] * 100, 2) }}%</h3>
                                    <p>Anak-anak</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card rounded-card left-border animate mt-3">
                        <div class="card-body text-center">
                            <h5>Ketercapaian Vaksin Dosis 3</h5>
                            <div class="row mt-5 text-center vaccine-data">
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_3']['Tenaga Kesehatan'] / $vaksin['sasaran_nakes'] * 100, 2) }}%</h3>
                                    <p>Tenaga Kesehatan</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_3']['Petugas Publik'] / $vaksin['sasaran_petugas_publik'] * 100, 2) }}%</h3>
                                    <p>Petugas Publik</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_3']['Lansia'] / $vaksin['sasaran_lansia'] * 100, 2) }}%</h3>
                                    <p>Lansia</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_3']['Masyarakat'] / $vaksin['sasaran_masyarakat'] * 100, 2) }}%</h3>
                                    <p>Masyarakat</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_3']['Remaja'] / $vaksin['sasaran_remaja'] * 100, 2) }}%</h3>
                                    <p>Remaja</p>
                                </div>
                                <div class="col-6">
                                    <h3>{{ number_format($vaksin['vaksin_3']['Anak'] / $vaksin['sasaran_anak'] * 100, 2) }}%</h3>
                                    <p>Anak-anak</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-12 col-lg-6">
                    <div class="card rounded-card left-border animate mt-3">
                        <div class="card-body text-center">
                            <h3 class="text-center">Total Dosis Vaksin</h3>
                            <p class="mt-n2">Data per tanggal {{ $date }}</p>
                            <div class="row mt-4 text-center">
                                <div class="col-4">
                                    <h3>{{ number_format(array_sum($vaksin['vaksin_1'])) }}</h3>
                                    <p>Dosis Pertama</p>
                                </div>
                                <div class="col-4">
                                    <h3>{{ number_format(array_sum($vaksin['vaksin_2'])) }}</h3>
                                    <p>Dosis Kedua</p>
                                </div>
                                <div class="col-4">
                                    <h3>{{ number_format(array_sum($vaksin['vaksin_3'])) }}</h3>
                                    <p>Dosis Ketiga</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="card rounded-card left-border animate mt-3">
                        <div class="card-body">
                            <h3 class="text-center">Total Vaksin Per Dosis</h3>
                            <div class="row mt-4 text-center">
                                
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 mt-3">
                            <div class="card case-counter animate rounded-card left-border">
                                <div class="card-body text-center">
                                    <h5>Grafik Vaksin Dosis Pertama, Kedua, dan Ketiga</h5>
                                    <canvas id="vaccine-dose" class="mt-4"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Worldwide Tracker Area =================-->
@endsection

@section('custom-js')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="{{ asset('assets/js/vaksin.js') }}"></script>
@endsection