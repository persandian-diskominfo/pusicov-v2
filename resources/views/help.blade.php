@extends('layouts.base')

@section('title', 'Bantuan')
@section('meta-description', 'Halaman pusat bantuan portal web resmi Pusat Informasi COVID-19 Kota Bandung. Info terkait kontak darurat COVID-19, dan daftar rumah sakit rujukan COVID-19 di Kota Bandung. Copyright © 2022 Pemerintah Kota Bandung.')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.5.css') }}">
@endsection

@section('main-content')
    <!--================Breadcrumb Area =================-->
    <section class="breadcrumb_area">
        <div class="container">
            <div class="breadcrumb_text text-left">
                <h3 class="animate">Pusat Bantuan COVID-19</h3>
                <h6 class="animate">Kota Bandung</h6>
                <ul class="nav">
                    <li class="animate"><a href="{{ url('') }}">Beranda</a></li>
                    <li class="animate">Bantuan</li>
                </ul>
            </div>
        </div>
    </section>
    <!--================End Breadcrumb Area =================-->
  
    <!--================Check Now Area =================-->
    <section class="check_now_area">
        <div class="container">
            <div class="row m-0 py-4 justify-content-between">
                <div class="left mx-auto mx-lg-0">
                    <div class="media d-flex flex-column justify-content-center animate">
                        <img src="{{ asset('assets/images/bcc.png') }}" alt="Bandung Command Center Logo" class="img-responsive" style="height: 14rem">
                    </div>
                </div>
                <div class="right mx-4 mx-md-0 text-white animate">
                    <h4 class="text-center text-lg-left">Posko Tim Tanggap COVID-19 &mdash; Bandung Command Center</h4>
                    <p class="m-0 p-0 py-1">
                        <i class="mr-1" data-feather="map-pin"></i>
                        Jl. Wastukencana No. 2 Bandung
                    </p>
                    <p class="m-0 p-0 py-1">
                        <i class="mr-1" data-feather="phone"></i>
                        Kegawatdaruratan: 119 atau 112
                    </p>
                    <p class="m-0 p-0 py-1">
                        <i class="mr-1" data-feather="phone"></i>
                        Info PSBB: 022-113
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!--================End Check Now Area =================-->

    <!--================Worldwide Tracker Area =================-->
    <section class="world_wide_tracker pt-5" style="background-color: rgba(0, 0, 0, .05)">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="card rounded-card left-border animate d-none d-lg-block">
                        <div class="card-body">
                            <h4 class="card-title">Peta Rumah Sakit Rujukan</h4>
                            <p>Berikut ini adalah rumah sakit yang menjadi rujukan untuk pasien dengan status Pasien dalam Pengawasan. Anda harus mengunjungi fasilitas kesehatan terdekat terlebih dahulu (misalnya klinik atau rumah sakit umum) sebelum akhirnya dapat dirujuk ke rumah sakit khusus di bawah ini.</p>
                            <div id="map" style="height: 600px" class="mt-3"></div>
                            <small>Klik salah satu Rumah Sakit pada peta di atas untuk melihat detailnya di Google Maps</small>
                        </div>
                    </div>

                    <div class="card rounded-card mt-3 left-border animate d-block d-lg-none">
                        <div class="card-body">
                            <h4 class="card-title pb-0 mb-0">Daftar Rumah Sakit Rujukan</h4>
                            <p class="d-block d-lg-none mt-3">Berikut ini adalah rumah sakit yang menjadi rujukan untuk pasien dengan status Pasien dalam Pengawasan. Anda harus mengunjungi fasilitas kesehatan terdekat terlebih dahulu (misalnya klinik atau rumah sakit umum) sebelum akhirnya dapat dirujuk ke rumah sakit khusus di bawah ini.</p>
                            <table class="table table-striped table-hover mt-3" id="list-rs">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th>Nama Rumah Sakit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <small class="d-block d-lg-none">Klik salah satu Rumah Sakit pada daftar di atas untuk melihat detailnya di Google Maps</small>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 mt-3 mt-lg-0">
                    <div class="card rounded-card left-border animate">
                        <div class="card-body">
                            <h4>Tautan Penting Lainnya</h4>
                            <ul class="pl-4 mt-3">
                                <li><a target="_blank" href="https://infeksiemerging.kemkes.go.id/">Kementrian Kesehatan</a></li>
                                <li><a target="_blank" href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019">World Health Organization (WHO)</a></li>
                                <li><a target="_blank" href="https://www.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6">Peta Kasus Global COVID-19</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Worldwide Tracker Area =================-->
@endsection

@section('custom-js')
<script src="{{ asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.5.min.js') }}"></script>
<script src="{{ asset('assets/js/json-map/bandung.js') }}"></script>
<script type="module" src="{{ asset('assets/js/help.js') }}"></script>
@endsection