@extends('layouts.base')

@section('title', 'Beranda')
@section('meta-description', 'Halaman utama portal web resmi Pusat Informasi COVID-19 Kota Bandung. Copyright © 2022 Pemerintah Kota Bandung.')

@section('main-content')
    <!--================Home Banner Area =================-->
    <section class="home_banner_area">
        <div class="container" style="padding-bottom: 4rem">
          <div class="row home_banner_inner">
            <div class="col-sm-7">
                <h3 class="animate">Pusat Informasi COVID-19</h3>
                <h5 class="animate">Kota Bandung</h5>
            </div>
            <div class="col-sm-12">
                <div class="row mt-5">
                    <div class="col-12 col-sm-6 col-md-4 mt-3 mt-md-0">
                        <div class="card rounded-card left-border animate py-0">
                            <div class="card-body d-flex flex-row align-items-center">
                                <div class="px-2">
                                    <ion-icon name="call-outline" size="large"></ion-icon>
                                </div>
                                <div class="col-10">
                                    <h4 class="card-subtitle mt-2">Call Center</h4>
                                    Nomor Darurat COVID-19
                                    <a href="tel:119">
                                        <h5 class="card-title mt-2 mb-1 fs-1">119</h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 mt-3 mt-md-0">
                        <div class="card rounded-card left-border animate py-0">
                            <div class="card-body d-flex flex-row align-items-center">
                                <div class="px-2">
                                    <ion-icon name="call-outline" size="large"></ion-icon>
                                </div>
                                <div class="col-10">
                                    <h4 class="card-subtitle mt-2">Call Center</h4>
                                    Nomor Darurat COVID-19
                                    <a href="tel:112">
                                        <h5 class="card-title mt-2 mb-1 fs-1">112</h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 mt-3 mt-md-0">
                        <div class="card rounded-card left-border animate py-0">
                            <div class="card-body d-flex flex-row align-items-center">
                                <div class="px-2">
                                    <ion-icon name="logo-whatsapp" size="large"></ion-icon>
                                </div>
                                <div class="col-10">
                                    <h4 class="card-subtitle mt-2">Chatbot WhatsApp</h4>
                                    Bandung Smart City
                                    <a href="https://wa.me/628112591810" target="_blank" rel="noopener noreferrer">
                                        <h5 class="card-title mt-2 mb-1 fs-1">0811 2591 810</h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </section>
      <!--================End Home Banner Area =================-->
  
      <!--================Check Now Area =================-->
      <section class="check_now_area">
        <div class="container">
          <div class="row m-0 justify-content-between">
            <div class="left">
              <div class="media">
                <div class="d-flex">
                  <img src="assets/images/check-1.png" alt="" class="animate" />
                  <img src="assets/images/check-2.png" alt="" class="animate" />
                  <img src="assets/images/check-3.png" alt="" class="animate" />
                </div>
                <div class="media-body mx-3">
                  <h2 class="text-white animate">Total Kasus Terkonfirmasi</h2>
                  <p class="animate">
                    Data per tanggal {{ $latest_updates['date'] }}
                  </p>
                </div>
              </div>
            </div>
            <div class="right mx-4 mx-md-0">
              <h1 class="text-white animate d-flex flex-row align-items-center">
                {{ number_format($latest_updates['total_terkonfirmasi']) }}
                <span class="text-danger d-flex flex-row align-items-start ml-3" style="font-size: 1.8rem">
                  <i data-feather="chevrons-up" class="mt-1"></i>
                  <span>{{ number_format($latest_updates['total_terkonfirmasi'] - $previous_updates['total_terkonfirmasi']) }}</span>
                </span>
              </h1>
            </div>
          </div>
        </div>
      </section>
      <!--================End Check Now Area =================-->
  
      <!--================Worldwide Tracker Area =================-->
      <section class="world_wide_tracker pt-5">
        <div class="container">
          <h1 class="mt-3 text-center animate">Kasus Harian</h1>
          <div class="row tracker_inner mt-5">
            <div class="col-lg-4 col-12">
              <div class="card case-counter animate rounded-card mt-3 mt-md-0">
                <div class="card-body d-flex flex-row">
                  <div class="d-flex justify-content-center align-items-center mx-3">
                    <img src="assets/images/icon/corona-red-1.png" alt="" />
                  </div>
                  <div class="d-flex flex-column justify-content-center">
                    <h2 id="total-active" class="flex-column">
                      {{ number_format($latest_updates['terkonfirmasi_aktif']) }}
                      <span class="text-danger">
                          <i data-feather="{{ $latest_updates['terkonfirmasi_aktif'] - $previous_updates['terkonfirmasi_aktif'] < 0 ? 'chevrons-down' : 'chevrons-up' }}"></i>
                          {{ number_format(abs($latest_updates['terkonfirmasi_aktif'] - $previous_updates['terkonfirmasi_aktif'])) }}
                      </span>
                    </h2>
                    <p>Konfirmasi Aktif</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-12">
              <div class="card case-counter animate rounded-card mt-3 mt-md-0">
                <div class="card-body d-flex flex-row">
                  <div class="d-flex justify-content-center align-items-center mx-3">
                    <img src="assets/images/icon/corona-green-1.png" alt="" />
                  </div>
                  <div class="d-flex flex-column justify-content-center">
                    <h2 id="total-active" class="flex-column">
                      {{ number_format($latest_updates['terkonfirmasi_sembuh']) }}
                      <span class="text-danger">
                          <i data-feather="{{ $latest_updates['terkonfirmasi_sembuh'] - $previous_updates['terkonfirmasi_sembuh'] < 0 ? 'chevrons-down' : 'chevrons-up' }}"></i>
                          {{ number_format(abs($latest_updates['terkonfirmasi_sembuh'] - $previous_updates['terkonfirmasi_sembuh'])) }}
                      </span>
                    </h2>
                    <p>Konfirmasi Sembuh</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-12">
              <div class="card case-counter animate rounded-card mt-3 mt-md-0">
                <div class="card-body d-flex flex-row">
                  <div class="d-flex justify-content-center align-items-center mx-3">
                    <img src="assets/images/icon/corona-black-1.png" alt="" />
                  </div>
                  <div class="d-flex flex-column justify-content-center">
                    <h2 id="total-active" class="flex-column">
                      {{ number_format($latest_updates['terkonfirmasi_meninggal']) }}
                      <span class="text-danger">
                          <i data-feather="{{ $latest_updates['terkonfirmasi_meninggal'] - $previous_updates['terkonfirmasi_meninggal'] < 0 ? 'chevrons-down' : 'chevrons-up' }}"></i>
                          {{ number_format(abs($latest_updates['terkonfirmasi_meninggal'] - $previous_updates['terkonfirmasi_meninggal'])) }}
                      </span>
                    </h2>
                    <p>Konfirmasi Meninggal</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row mt-5 pt-4">
              <div class="col-12 d-flex justify-content-center">
                <a href="{{ url('detail-kasus') }}" class="btn btn-outline-secondary animate d-flex flex-row align-items-center">Lihat Selengkapnya <i data-feather="chevron-right"></i></a>
              </div>
          </div>
        </div>
      </section>
      <!--================End Worldwide Tracker Area =================-->
@endsection