@extends('layouts.base')

@section('title', 'Frequently Asked Question')
@section('meta-description', 'Halaman FAQ portal web resmi Pusat Informasi COVID-19 Kota Bandung. Lihat pertanyaan yang sering ditanyakan, pertanyaan dasar terkait pandemi COVID-19. Copyright © 2022 Pemerintah Kota Bandung.')

@section('main-content')
    <!--================Breadcrumb Area =================-->
    <section class="breadcrumb_area">
        <div class="container">
            <div class="breadcrumb_text text-left">
                <h3 class="animate">Frequently Asked Question</h3>
                <h6 class="animate">COVID-19</h6>
                <ul class="nav">
                    <li class="animate"><a href="{{ url('') }}">Beranda</a></li>
                    <li class="animate">FAQ</li>
                </ul>
            </div>
        </div>
    </section>
    <!--================End Breadcrumb Area =================-->

    <!--================Start Faq Area =================-->
    <section class="about_faq_area about_cronacvirus_promo_area">
        <div class="container">
          <div class="bg_img">
              <img class="shap d-none d-lg-block animate" src="assets/images/about/about_promo_bg.png" alt="">
          </div>
          <div class="row pb-5">
            <div class="col-lg-6 offset-lg-6">
              <div class="question_list faq_inner">
                <div class="accordion" id="accordionExample">
                  <div class="card animate">
                    <div class="card-header" id="headingOne">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne"
                        aria-expanded="false" aria-controls="collapseOne">
                        Apakah Coronavirus dan COVID-19 itu?
                        <i data-feather="chevron-right"></i>
                      </button>
                    </div>
  
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                      data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="faq_content">
                          <p>
                            Coronavirus merupakan keluarga besar virus yang menyebabkan penyakit pada manusia dan hewan. Pada manusia biasanya menyebabkan penyakit infeksi saluran pernapasan, mulai flu biasa hingga penyakit yang serius seperti Middle East Respiratory Syndrome (MERS) dan Sindrom Pernafasan Akut Berat/ Severe Acute Respiratory Syndrome (SARS). Coronavirus jenis baru yang ditemukan pada manusia sejak kejadian luar biasa muncul di Wuhan Cina, pada Desember 2019, kemudian diberi nama Severe Acute Respiratory Syndrome Coronavirus 2 (SARS-CoV-2), dan menyebabkan penyakit Coronavirus Disease-2019 (COVID-19).
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card animate">
                    <div class="card-header" id="headingTwo">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Siapa yang termasuk suspek?
                        <i data-feather="chevron-right"></i>
                      </button>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="faq_content">
                          <p>
                            Suspek COVID-19 adalah orang yang mengalami gejala demam (>38 C) atau ada riwayat demam ATAU ISPA tanpa pneumonia DAN memiliki riwayat perjalanan ke negara atau daerah yang dikategorikan transmisi lokal COVID-19 pada 14 hari terakhir sebelum timbul gejala.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card animate">
                    <div class="card-header" id="headingThree">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Siapa yang termasuk kontak erat?
                        <i data-feather="chevron-right"></i>
                      </button>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                      data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="faq_content">
                          <p>
                            Orang yang memiliki riwayat kontak dengan kasus probable atau konfirmasi COVID-19
                            <ul>
                                <li>Kontak tatap muka/berdekatan dengan orang yang kasus probable atau kasus konfirmasi dalam radius 1 meter dan dalam jangka waktu 15 menit atau lebih</li>
                                <li>Sentuhan fisik langsung dengan kasus probable atau konfirmasi (seperti bersalaman, berpegangan tangan, dan lain-lain)</li>
                                <li>Orang yang memberikan perawatan langsung terhadap kasus probable atau konfirmasi tanpa menggunakan APD yang sesuai</li>
                                <li>Situasi lainnya yang mengindikasikan adanya kontak berdasarkan penilaian risiko lokal yang ditetapkan oleh tim penyelidikan epidemiologi setempat</li>
                            </ul>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card animate">
                    <div class="card-header" id="headingfour">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                        Siapa yang termasuk orang konfirmasi positif COVID-19?
                        <i data-feather="chevron-right"></i>
                      </button>
                    </div>
                    <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="faq_content">
                          <p>
                            Seseorang yang dinyatakan Positif Terinfeksi Virus COVID-19 yang dibuktikan dengan pemeriksaan RT-PCR.<br>
                            Kasus Konfirmasi terbagi dua kelompok:<br>
                            <b>SIMPTOMATIK</b><br>
                            Kasus Konfirmasi Dengan Gejala<br>
                            <b>ASIMPTOMATIK</b><br>
                            Kasus Konfirmasi Tanpa Gejala
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card animate">
                    <div class="card-header" id="headingfive">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                        Bagaimana cara mengantisipasi penularan COVID-19?
                        <i data-feather="chevron-right"></i>
                      </button>
                    </div>
                    <div id="collapsefive" class="collapse" aria-labelledby="headingfive" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="faq_content">
                          <p>
                            Anda dapat melakukan tindakan pencegahan agar tidak tertular diantaranya dengan cara berikut ini.
                            <ul>
                                <li>Menjaga kesehatan dan kebugaran agar sistem imunitas/kekebalan tubuh meningkat.</li>
                                <li>Mencuci tangan menggunakan sabun dan air mengalir atau menggunakan alkohol 70-80% handrub sesuai langkah-langkah mencuci tangan yang benar. Mencuci tangan sampai bersih merupakan salah satu tindakan yang mudah dan murah. Dan, sebagian besar penyebaran penyakit akibat virus dan bakteri bersumber dari tangan. Karena itu, menjaga kebersihan tangan adalah hal yang sangat penting.</li>
                                <li>Ketika batuk dan bersin, upayakan menjaga agar lingkungan Anda tidak tertular. Tutup hidung dan mulut Anda dengan tisu atau dengan lipatan siku tangan bagian dalam (bukan dengan telapak tangan) dan gunakan masker.</li>
                                <li>Gunakan masker penutup mulut dan hidung (tipe masker bedah) ketika Anda sakit, dan masker harus diganti secara berkala. Masker cuci pakai, tidak diperkenankan.</li>
                                <li>Buang tisu dan masker yang sudah digunakan ke tempat sampah dan cucilah tangan Anda.</li>
                                <li>Hindari konsumsi protein berbahan dasar hewani yang tidak diolah (mentah)/diolah setengah matang.</li>
                                <li>Jika Anda berencana berkunjung ke daerah/negara dengan risiko tinggi COVID-19, terapkan perilaku hidup bersih dan sehat untuk menjaga daya tahan tubuh Anda agar terhindar dari infeksi Coronavirus Jika Anda mengalami gejala mirip dengan gejala COVID-19 sepulang dari daerah atau negara dengan risiko tinggi COVID-19, Anda tidak perlu panik. Segera periksa ke fasilitas kesehatan terdekat dan beritahukan kepada petugas kesehatan tentang riwayat perjalanan Anda. Atau menghubungi Posko COVID-19 di nomor 112.</li>
                            </ul>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card animate">
                    <div class="card-header" id="headingsix">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                        Apakah gejala yang saya alami termasuk gejala COVID-19?
                        <i data-feather="chevron-right"></i>
                      </button>
                    </div>
                    <div id="collapsesix" class="collapse" aria-labelledby="headingsix" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="faq_content">
                          <p>
                            Apakah Anda mengalami Gejala COVID-19 sebagai berikut:
                            <ul>
                                <li>Demam suhu tinggi</li>
                                <li>Batuk</li>
                                <li>Pilek</li>
                                <li>Nyeri tenggorokan</li>
                                <li>Sesak Nafas</li>
                            </ul>
                            Jika ada gejala di atas DAN ada riwayat perjalanan dari negara terjangkit COVID-19 atau Anda terpapar dengan pasien Positif COVID-19, hubungi nomor darurat tanggap COVID-19 Bandung Command Center di 112 untuk mendapat arahan lebih lanjut.<br>
                            Anda juga bisa lakukan cek mandiri di halaman <a href="{{ url('cek-mandiri') }}">Cek Mandiri COVID-19</a>.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card animate">
                    <div class="card-header" id="headingseven">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
                        Sepertinya saya berinteraksi dengan yang terkonfirmasi COVID-19, apa yang harus saya lakukan?
                        <i data-feather="chevron-right"></i>
                      </button>
                    </div>
                    <div id="collapseseven" class="collapse" aria-labelledby="headingseven" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="faq_content">
                          <p>
                            Segera hubungi nomor darurat tanggap COVID-19 Bandung Command Center di <a href="tel:119">119</a> atau <a href="tel:112">112</a> atau Dinas Kesehatan.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card animate">
                    <div class="card-header" id="headingeight">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">
                        Bisakah antibiotik menyembuhkan atau mencegah penularan COVID-19?
                        <i data-feather="chevron-right"></i>
                      </button>
                    </div>
                    <div id="collapseeight" class="collapse" aria-labelledby="headingeight" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="faq_content">
                          <p>
                            Antibiotik tidak untuk penyakit yang diakibatkan oleh virus jenis apapun, termasuk coronavirus. Antibiotik mungkin diberikan kepada beberapa orang yang terjangkit COVID-19 dengan komplikasi infeksi bakteri.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card animate">
                    <div class="card-header" id="headingnine">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                        data-target="#collapsenine" aria-expanded="false" aria-controls="collapsenine">
                        Apakah COVID-19 hanya menular pada orang tua saja?
                        <i data-feather="chevron-right"></i>
                      </button>
                    </div>
                    <div id="collapsenine" class="collapse" aria-labelledby="headingnine" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="faq_content">
                          <p>
                            Semua orang dari berbagai usia, berisiko terinfeksi coronavirus. Pada lansia dan orang-orang dengan kondisi medis yang sudah ada sebelumnya (seperti asma, diabetes, penyakit jantung) lebih cenderung mengalami kondisi yang parah atau komplikasi yang berat.
                          </p>
                          <p>
                            Semua orang harus mengikuti langkah-langkah sederhana untuk menghentikan virus seperti penyebaran virus corona, misalnya dengan sering mencuci tangan dengan sabun dan air.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--================End Faq Area =================-->
@endsection