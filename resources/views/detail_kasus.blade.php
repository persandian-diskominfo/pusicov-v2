@extends('layouts.base')

@section('title', 'Detail Kasus')
@section('meta-description', 'Halaman detail kasus portal web resmi Pusat Informasi COVID-19 Kota Bandung. Lihat total kasus terkonfirmasi, terkonfirmasi aktif, terkonfirmasi sembuh, terkonfirmasi meninggal, suspek, dan kontak erat. Copyright © 2022 Pemerintah Kota Bandung.')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.5.css') }}">
@endsection

@section('main-content')
    <!--================Breadcrumb Area =================-->
    <section class="breadcrumb_area">
        <div class="container">
            <div class="breadcrumb_text text-left">
                <h3 class="animate">Detail Kasus COVID-19</h3>
                <h6 class="animate">Kota Bandung</h6>
                <ul class="nav">
                    <li class="animate"><a href="{{ url('') }}">Beranda</a></li>
                    <li class="animate">Detail Kasus</li>
                </ul>
            </div>
        </div>
    </section>
    <!--================End Breadcrumb Area =================-->

    <!--================Check Now Area =================-->
    <section class="check_now_area">
        <div class="container">
            <div class="row m-0 justify-content-between">
                <div class="left">
                    <div class="media">
                        <div class="d-flex">
                            <img src="assets/images/check-1.png" alt="" class="animate" />
                            <img src="assets/images/check-2.png" alt="" class="animate" />
                            <img src="assets/images/check-3.png" alt="" class="animate" />
                        </div>
                        <div class="media-body mx-3">
                            <h2 class="text-white animate">Total Kasus Terkonfirmasi</h2>
                            <p class="animate">
                                Data per tanggal {{ $latest_updates['date'] }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="right mx-4 mx-sm-0">
                    <h1 class="text-white animate d-flex flex-row align-items-center">
                        {{ number_format($latest_updates['total_terkonfirmasi']) }}
                        <span class="text-danger d-flex flex-row align-items-start ml-3" style="font-size: 1.8rem">
                            <i data-feather="{{ $latest_updates['total_terkonfirmasi'] - $previous_updates['total_terkonfirmasi'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mt-1"></i>
                            <span>{{ number_format(abs($latest_updates['total_terkonfirmasi'] - $previous_updates['total_terkonfirmasi'])) }}</span>
                        </span>
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!--================End Check Now Area =================-->

    <!--================Worldwide Tracker Area =================-->
    <section class="world_wide_tracker pt-5" style="background-color: rgba(0, 0, 0, .05)">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-4 mt-3">
                            <div class="card case-counter animate rounded-card left-border">
                                <div class="card-body">
                                    <div class="d-flex flex-column align-items-center px-2">
                                        <h2 id="total-active">
                                            {{ number_format($latest_updates['terkonfirmasi_aktif']) }}
                                            <span class="text-danger ml-2">
                                                <i data-feather="{{ $latest_updates['terkonfirmasi_aktif'] - $previous_updates['terkonfirmasi_aktif'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n1"></i>
                                                {{ number_format(abs($latest_updates['terkonfirmasi_aktif'] - $previous_updates['terkonfirmasi_aktif'])) }}
                                            </span>
                                        </h2>
                                        <p>Konfirmasi Aktif</p>
                                    </div>
                                    <div class="absolute">
                                        <img src="{{ asset('assets/images/chevrons-up.svg') }}" width="200px" height="200px" class="no-rotate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 mt-3">
                            <div class="card case-counter animate rounded-card left-border">
                                <div class="card-body">
                                    <div class="d-flex flex-column align-items-center px-2">
                                        <h2 id="total-active">
                                            {{ number_format($latest_updates['terkonfirmasi_sembuh']) }}
                                            <span class="text-danger ml-2">
                                                <i data-feather="{{ $latest_updates['terkonfirmasi_sembuh'] - $previous_updates['terkonfirmasi_sembuh'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n1"></i>
                                                {{ number_format(abs($latest_updates['terkonfirmasi_sembuh'] - $previous_updates['terkonfirmasi_sembuh'])) }}
                                            </span>
                                        </h2>
                                        <p>Konfirmasi Sembuh</p>
                                    </div>
                                    <div class="absolute">
                                        <img src="{{ asset('assets/images/chevrons-up.svg') }}" width="200px" height="200px" class="no-rotate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4 mt-3">
                            <div class="card case-counter animate rounded-card left-border">
                                <div class="card-body">
                                    <div class="d-flex flex-column align-items-center px-2">
                                        <h2 id="total-active">
                                            {{ number_format($latest_updates['terkonfirmasi_meninggal']) }}
                                            <span class="text-danger ml-2">
                                                <i data-feather="{{ $latest_updates['terkonfirmasi_meninggal'] - $previous_updates['terkonfirmasi_meninggal'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n1"></i>
                                                {{ number_format(abs($latest_updates['terkonfirmasi_meninggal'] - $previous_updates['terkonfirmasi_meninggal'])) }}
                                            </span>
                                        </h2>
                                        <p>Konfirmasi Meninggal</p>
                                    </div>
                                    <div class="absolute">
                                        <img src="{{ asset('assets/images/chevrons-up.svg') }}" width="200px" height="200px" class="no-rotate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 mt-3">
                            <div class="card case-counter animate rounded-card left-border">
                                <div class="card-body">
                                    <h4 class="text-center">Suspek</h4>
                                    <div class="row mt-4 text-center">
                                        <div class="col-4">
                                            <h3>{{ number_format($latest_updates['total_suspek']) }}</h3>
                                            <p class="text-danger mt-n2">
                                                <i data-feather="{{ $latest_updates['total_suspek'] - $previous_updates['total_suspek'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                                {{ number_format(abs($latest_updates['total_suspek'] - $previous_updates['total_suspek'])) }}
                                            </p>
                                            <p>Total</p>
                                        </div>
                                        <div class="col-4">
                                            <h3>{{ number_format($latest_updates['suspek_dalam_pantauan']) }}</h3>
                                            <p class="text-danger mt-n2">
                                                <i data-feather="{{ $latest_updates['suspek_dalam_pantauan'] - $previous_updates['suspek_dalam_pantauan'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                                {{ number_format(abs($latest_updates['suspek_dalam_pantauan'] - $previous_updates['suspek_dalam_pantauan'])) }}
                                            </p>
                                            <p>Dipantau</p>
                                        </div>
                                        <div class="col-4">
                                            <h3>{{ number_format($latest_updates['suspek_discarded']) }}</h3>
                                            <p class="text-danger mt-n2">
                                                <i data-feather="{{ $latest_updates['suspek_discarded'] - $previous_updates['suspek_discarded'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                                {{ number_format(abs($latest_updates['suspek_discarded'] - $previous_updates['suspek_discarded'])) }}
                                            </p>
                                            <p>Discarded</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 mt-3">
                            <div class="card case-counter animate rounded-card left-border">
                                <div class="card-body">
                                    <h4 class="text-center">Kontak Erat</h4>
                                    <div class="row mt-4 text-center">
                                        <div class="col-4">
                                            <h3>{{ number_format($latest_updates['total_kontak_erat']) }}</h3>
                                            <p class="text-danger mt-n2">
                                                <i data-feather="{{ $latest_updates['total_kontak_erat'] - $previous_updates['total_kontak_erat'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                                {{ number_format(abs($latest_updates['total_kontak_erat'] - $previous_updates['total_kontak_erat'])) }}
                                            </p>
                                            <p>Total</p>
                                        </div>
                                        <div class="col-4">
                                            <h3>{{ number_format($latest_updates['kontak_erat_dalam_pantauan']) }}</h3>
                                            <p class="text-danger mt-n2">
                                                <i data-feather="{{ $latest_updates['kontak_erat_dalam_pantauan'] - $previous_updates['kontak_erat_dalam_pantauan'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                                {{ number_format(abs($latest_updates['kontak_erat_dalam_pantauan'] - $previous_updates['kontak_erat_dalam_pantauan'])) }}
                                            </p>
                                            <p>Dipantau</p>
                                        </div>
                                        <div class="col-4">
                                            <h3>{{ number_format($latest_updates['kontak_erat_discarded']) }}</h3>
                                            <p class="text-danger mt-n2">
                                                <i data-feather="{{ $latest_updates['kontak_erat_discarded'] - $previous_updates['kontak_erat_discarded'] < 0 ? 'chevrons-down' : 'chevrons-up' }}" class="mr-n2 mt-n1"></i>
                                                {{ number_format(abs($latest_updates['kontak_erat_discarded'] - $previous_updates['kontak_erat_discarded'])) }}
                                            </p>
                                            <p>Discarded</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-12 col-lg-8 mt-3">
                    <div class="card animate rounded-card left-border">
                        <div class="card-body" style="height: 600px">
                            <h5 class="text-center">Grafik Kasus COVID-19</h5>
                            <canvas id="chart-kota" class="mt-4 pb-3"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 mt-3">
                    <div class="card animate rounded-card left-border">
                        <div class="card-body">
                            <h5 class="text-center">10 Wilayah dengan Kasus Aktif Tertinggi</h5>
                            <ul class="nav nav-pills nav-fill mt-3" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" id="kecamatan-tab" data-toggle="tab" href="#kecamatan" role="tab" aria-controls="kecamatan" aria-selected="true">Kecamatan</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="kelurahan-tab" data-toggle="tab" href="#kelurahan" role="tab" aria-controls="kelurahan" aria-selected="false">Kelurahan</a>
                                </li>
                            </ul>
                            <div class="tab-content mt-3" id="myTabContent">
                                <div class="tab-pane fade show active" id="kecamatan" role="tabpanel" aria-labelledby="kecamatan-tab" style="height: 380px">
                                    <canvas id="cases-kecamatan"></canvas>
                                </div>
                                
                                <div class="tab-pane fade" id="kelurahan" role="tabpanel" aria-labelledby="kelurahan-tab" style="height: 380px">
                                    <canvas id="cases-kelurahan"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Worldwide Tracker Area =================-->
@endsection

@section('custom-js')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="{{ asset('assets/js/detail-kasus.js') }}"></script>
@endsection