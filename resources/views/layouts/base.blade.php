<!DOCTYPE html>
<html lang="en">

	<head>
		<base href="{{ url('') }}" id="base-url">
		<!-- Required meta tags -->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<meta name="description" content="@yield('meta-description', 'Portal Resmi Pusat Informasi COVID-19 Kota Bandung. Copyright © 2022 Pemerintah Kota Bandung.')">
		<link rel="icon" href="{{ asset('assets/images/app-logo.svg') }}" type="image/png" />
		<title>
			@yield('title') &mdash; Pusat Informasi COVID-19 Kota Bandung
		</title>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
		{{-- <link rel="stylesheet" href="{{ asset('assets/vendors/mCustomScrollbar/jquery.mCustomScrollbar.min.css') }}" /> --}}
		
		<!-- main css -->
		<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}" />
		
		@yield('custom-css')
	</head>

	<body>
		<div class="body_wrapper">
			
			<!--================Preloader Area =================-->
			<div class="preloader">
				<div class="three-bounce">
					<div class="one"></div>
					<div class="two"></div>
					<div class="three"></div>
				</div>
			</div>
			<!--================End Preloader Area =================-->
			
			<!--================Mobile Canvus Menu Area =================-->
			<div class="mobile_canvus_menu">
				<div class="close_btn">
					<img src="assets/images/icon/close-dark.png" alt="">
				</div>
				<div class="menu_part_lux">
					<ul class="menu_list wd_scroll">
						<li><a href="{{ url('/') }}">Beranda</a></li>
						<li>
							<a href="#">
								Data
								<i data-feather="chevron-down"></i>
							</a>
							<ul class="list">
								<li><a href="{{ url('detail-kasus') }}">Kasus COVID-19</a></li>
								<li><a href="{{ url('vaksinasi') }}">Vaksinasi COVID-19</a></li>
							</ul>
						</li>
						<li><a href="{{ url('/peta-persebaran') }}">Peta Persebaran</a></li>
						<li><a href="{{ url('/faq') }}">FAQ</a></li>
						<li><a href="{{ url('/help') }}">Bantuan</a></li>
					</ul>
				</div>
				<div class="menu_btm">
					<a class="green_btn" href="{{ url('cek-mandiri') }}"><i data-feather="activity" class="mr-1 ml-n2"></i> Cek Mandiri</a>
				</div>
			</div>
			<!--================End Mobile Canvus Menu Area =================-->
			
			<!--================Header Area =================-->
			<header class="header_area">
				<div class="main_menu">
					<div class="container">
						<nav class="navbar navbar-expand-lg navbar-light bg-light">
							<a class="navbar-brand" href="{{ url('') }}">
								<img class="animate" src="{{ asset('assets/images/app-logo.svg') }}" height="50px" />
							</a>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="nav navbar-nav ml-auto animate">
								<li><a href="{{ url('/') }}">Beranda</a></li>
								<li class="dropdown submenu">
									<a href="#" data-toggle="dropdown" class="dropdown-toggle ">Data</a>
									<i data-feather="chevron-down"></i>
									<ul class="dropdown-menu">
										<li><a href="{{ url('/detail-kasus') }}">Kasus COVID-19</a></li>
										<li><a href="{{ url('/vaksinasi') }}">Vaksinasi COVID-19</a></li>
									</ul>
								</li>
								<li><a href="{{ url('/peta-persebaran') }}">Peta Persebaran</a></li>
								<li><a href="{{ url('/faq') }}">FAQ</a></li>
								<li><a href="{{ url('/help') }}">Bantuan</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li class="checker_btn animate">
									<a href="{{ url('cek-mandiri') }}">
										<i data-feather="activity" class="mr-1 ml-n2"></i>
										Cek Mandiri
									</a>
								</li>
							</ul>
						</div>
					</nav>
				</div>
				<div class="right_burger d-block d-md-none">
					<ul class="nav">
						<li>
							<div class="menu_btn">
								<img src="assets/images/icon/burger.png" alt="" />
							</div>
						</li>
					</ul>
				</div>
			</div>
		</header>
		<!--================End Header Area =================-->
		
		{{-- BEGIN Main Content --}}
		@yield('main-content')
		{{-- END of Main Content --}}
		
		<!--================App Area =================-->
		<section class="app_area @if(url()->current() === url('cek-mandiri')) d-none @endif">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<div class="app_text">
							<h2 class="animate">Unduh aplikasi <span>PUSICOV</span>!</h2>
							<p class="animate">
								Unduh aplikasi PUSICOV Bandung untuk mendapatkan akses informasi seputar COVID-19 dengan lebih mudah
							</p>
							<a class="animate" href="https://play.google.com/store/apps/details?id=gov.bdg.pusicovbandung" target="_blank"><img src="assets/images/google-btn.png" alt="" /></a>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="app_mobile">
							<div class="mobile_image">
								<img class="animate" src="assets/images/mobile-1.png" alt="" />
								<img class="animate" src="assets/images/mobile-2.png" alt="" />
							</div>
							<ul class="corona_img nav">
								<li>
									<img src="assets/images/icon/app-virus-1.png" alt="" />
								</li>
								<li>
									<img src="assets/images/icon/app-virus-2.png" alt="" />
								</li>
								<li>
									<img src="assets/images/icon/app-virus-3.png" alt="" />
								</li>
								<li data-parallax='{"y": -100}'>
									<img src="assets/images/icon/app-virus-4.png" alt="" />
								</li>
								<li data-parallax='{"y": 100}'>
									<img src="assets/images/icon/app-virus-5.png" alt="" />
								</li>
								<li>
									<img src="assets/images/icon/app-virus-6.png" alt="" />
								</li>
								<li data-parallax='{"y": -200}'>
									<img src="assets/images/icon/app-virus-7.png" alt="" />
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--================End App Area =================-->
		
		<!--================Footer Area =================-->
		<div class="footer_copyright">
			<div class="container">
				<img src="assets/images/app-logo.svg" alt="" style="opacity: .6" class="animate">
				<p class="animate">© Copyright
					<script>
						document.write(new Date().getFullYear());
					</script>
					Pemerintah Kota Bandung
				</p>
			</div>
		</div>
		<!--================End Footer Area =================-->
	</div>

		@yield('modal-section')

		<script src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>
		<script src="{{ asset('assets/js/popper.min.js') }}"></script>
		<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
		{{-- <script src="{{ asset('assets/vendors/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script> --}}
		<script src="https://unpkg.com/scrollreveal@4.0.0/dist/scrollreveal.min.js"></script>
		<script src="https://unpkg.com/feather-icons"></script>
		<script src="{{ asset('assets/js/theme.js') }}"></script>
		@yield('custom-js')
	</body>
</html>