<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dashboard &mdash; Pusat Informasi COVID-19 Kota Bandung</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/app-logo.svg') }}">

    <!-- page css -->
    <link href="{{ asset('assets/dashboard/vendors/apexcharts/dist/apexcharts.css') }}" rel="stylesheet">

    <!-- Core css -->
    <link href="{{ asset('assets/dashboard/css/app.min.css') }}" rel="stylesheet">

</head>

<body>
    <div class="layout">
        <div class="vertical-layout">
            <!-- Header START -->
            <div class="header-text-dark header-nav layout-vertical">
                <div class="header-nav-wrap">
                    <div class="header-nav-left">
                        <div class="header-nav-item desktop-toggle">
                            <div class="header-nav-item-select cursor-pointer">
                                <i class="nav-icon feather icon-menu icon-arrow-right"></i>
                            </div>
                        </div>
                        <div class="header-nav-item mobile-toggle">
                            <div class="header-nav-item-select cursor-pointer">
                                <i class="nav-icon feather icon-menu icon-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                    <div class="header-nav-right">
                        <div class="header-nav-item">
                            <div class="header-nav-item-select">
                                    {{-- <i class="nav-icon feather icon-settings"></i> --}}
                                Diskominfo Kota Bandung
                            </div>
                        </div>  
                    </div>
                </div>
            </div>    
            <!-- Header END -->

            <!-- Side Nav START -->
            <div class="side-nav vertical-menu nav-menu-light scrollable">
                <div class="nav-logo">
                    <div class="w-100 logo d-flex justify-content-center">
                        <img class="img-fluid" src="{{ asset('assets/images/app-logo.svg') }}" style="max-height: 50px;" alt="logo">
                    </div>
                    <div class="mobile-close">
                        <i class="icon-arrow-left feather"></i>
                    </div>
                 </div>
                 <ul class="nav-menu mt-3">
                    <li class="nav-menu-item router-link-active">
                        <a href="{{ url('/dashboard') }}">
                            <i class="feather icon-home"></i>
                            <span class="nav-menu-item-title">Dashboard</span>
                        </a>
                    </li>
                    {{-- <li class="nav-group-title">Import Data Manual</li>
                    <li class="nav-menu-item">
                        <a href="">
                            <i class="feather icon-cloud"></i>
                            <span class="nav-menu-item-title">Import dari Data Bandung</span>
                        </a>
                    </li>
                    <li class="nav-menu-item">
                        <a href="">
                            <i class="feather icon-file-text"></i>
                            <span class="nav-menu-item-title">Import dari Excel</span>
                        </a>
                    </li>
                    <li class="divider"></li> --}}
                    <li class="nav-menu-item">
                        <a href="{{ url('logout') }}">
                            <i class="feather icon-log-out"></i>
                            <span class="nav-menu-item-title">Keluar</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- Side Nav END -->

            <!-- Content START -->
            <div class="content">
                <div class="main">
                    @yield('main-content')
                </div>
                <!-- Footer START -->
                <div class="footer">
                    <div class="footer-content">
                        <p class="mb-0">Copyright © 2022 Pemerintah Kota Bandung. All rights reserved.</p>
                    </div>
                </div>
                <!-- Footer End -->
            </div>
            <!-- Content END -->
        </div>
    </div>

    
    <!-- Core Vendors JS -->
    <script src="{{ asset('assets/dashboard/js/vendors.min.js') }}"></script>

    <!-- page js -->

    <!-- Core JS -->
    <script src="{{ asset('assets/dashboard/js/app.min.js') }}"></script>

</body>

</html>