export default [{},
    {
        name: 'RS Hasan Sadikin',
        latLng: [-6.898144352142063, 107.59840235641268],
        status: 'pusat',
        address: 'Jl. Pasteur No.38, Pasteur, Kec. Sukajadi, Kota Bandung, Jawa Barat 40161',
        maps: 'https://goo.gl/maps/VCwhjDgH5gFX49N29'
    }, {
        name: 'RS TNI AU Dr. M. Salamun',
        latLng: [-6.86405171856092, 107.60482237597739],
        status: 'prov',
        address: 'Ciumbuleuit, Kec. Cidadap, Kota Bandung, Jawa Barat',
        maps: 'https://goo.gl/maps/9zjwznpRfk1pEUzM7'
    }, {
        name: 'RSTP Dr. H. A. Rotinsulu',
        latLng: [-6.87893380786156, 107.60602886895221],
        status: 'pusat',
        address: 'Jl. Bukit Jarian No.40, Hegarmanah, Kec. Cidadap, Kota Bandung, Jawa Barat 40141',
        maps: 'https://goo.gl/maps/WQziLecJqQKq8X897'
    }, {
        name: 'RS Bhayangkara Sartika Asih',
        latLng: [-6.956107303305812, 107.61220662477479],
        status: 'prov',
        address: 'Ciseureuh, Kec. Regol, Kota Bandung, Jawa Barat',
        maps: 'https://goo.gl/maps/BwXUtDqTNk1GwJvZ8'
    }, {
        name: 'RSUD Kota Bandung',
        latLng: [-6.915660344220025, 107.6987196401168],
        status: 'kota',
        address: 'Jl. Rumah Sakit No.22, Pakemitan, Kec. Cinambo, Kota Bandung, Jawa Barat 45474',
        maps: 'https://goo.gl/maps/WB3H96yvTeAYBMwz7'
    }, {
        name: 'RSKIA Kota Bandung',
        latLng: [-6.9292585721382585, 107.60050392530427],
        status: 'kota',
        address: 'Jl. Astana Anyar No.224, Nyengseret, Kec. Astanaanyar, Kota Bandung, Jawa Barat 40242',
        maps: 'https://goo.gl/maps/9aNbQgfowbg5Z2gE8'
    }, {
        name: 'RS Advent',
        latLng: [-6.89182094107243, 107.60323284011676],
        status: 'kota',
        address: 'Jl. Cihampelas No.161, Cipaganti, Kecamatan Coblong, Kota Bandung, Jawa Barat 40131',
        maps: 'https://g.page/RsAdventBandung?share'
    }, {
        name: 'RS Santo Borromeus',
        latLng: [-6.893952889948939, 107.6136689689523],
        status: 'kota',
        address: 'Jl. Ir. H. Juanda No.100, Lebakgede, Kecamatan Coblong, Kota Bandung, Jawa Barat 40132',
        maps: 'https://goo.gl/maps/m6GBhpqWi2sF8Wxw6'
    }, {
        name: 'RS Immanuel',
        latLng: [-6.935976990223544, 107.59632093059662],
        status: 'kota',
        address: 'Jl. Raya Kopo No.161, Situsaeur, Kec. Bojongloa Kidul, Kota Bandung, Jawa Barat 40233',
        maps: 'https://g.page/rsimmanuelbdg?share'
    }, {
        name: 'RS Hermina Arcamanik',
        latLng: [-6.904866390923179, 107.66673826895241],
        status: 'kota',
        address: 'Jl. A.H. Nasution No.50, Antapani Wetan, Kec. Antapani, Kota Bandung, Jawa Barat 40291',
        maps: 'https://g.page/rsherminaarcamanik?share'
    }, {
        name: 'RS Al-Islam Bandung',
        latLng: [-6.93898599447965, 107.66916516895263],
        status: 'kota',
        address: 'Jl. Soekarno-Hatta No.644, Manjahlega, Kec. Rancasari, Kota Bandung, Jawa Barat 40286',
        maps: 'https://goo.gl/maps/7BVmBwxvWPqpntFj9'
    }, {
        name: 'RS Santosa Central',
        latLng: [-6.915474493434326, 107.60036526895261],
        status: 'kota',
        address: 'Jl. Kebon Jati No.38, Kb. Jeruk, Kec. Andir, Kota Bandung, Jawa Barat 40181',
        maps: 'https://goo.gl/maps/Ykh6txRrrofcytXY9'
    }, {
        name: 'RS Santosa Kopo',
        latLng: [-6.952611345029251, 107.58637189778851],
        status: 'tambahan',
        address: 'Jl. Raya Kopo No.461-463, Cirangrang, Kec. Babakan Ciparay, Kota Bandung, Jawa Barat 40224',
        maps: 'https://goo.gl/maps/oCWZcNN9KbV794p3A'
    }, {
        name: 'RS Santo Yusup',
        latLng: [-6.906653893043076, 107.64310632662358],
        status: 'tambahan',
        address: 'Jl. Cikutra No.7, Cikutra, Kec. Cibeunying Kidul, Kota Bandung, Jawa Barat 40124',
        maps: 'https://goo.gl/maps/WrKZsB2jyrsvctYy6'
    }, {
        name: 'RSIA Limijati',
        latLng: [-6.906277591049309, 107.61366196895239],
        status: 'tambahan',
        address: 'Jalan, LLRE Martadinata St No.39, Citarum, Bandung Wetan, Bandung City, West Java 40115',
        maps: 'https://g.page/Limijati?share'
    }
]