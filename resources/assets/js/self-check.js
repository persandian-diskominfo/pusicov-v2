$(function() {
    let answers = []

    $('.moveDown').click(function() {
        answers.push($(this).find('input[type="radio"]').attr('value'))
    })

    $('.moveDown.last').click(function() {
        $(`#${answers.includes('ya') ? 'potential' : 'impotential'}-modal`).modal('show')
    })
})