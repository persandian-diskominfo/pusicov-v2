$(function() {
    $('.moveDown').click(function() {
        const value = $(this).find('input[type="radio"]').attr('value')

        if ($(this).hasClass('last')) {
            value === 'tidak' ? $('#improbable-modal').modal('show') : null
        }

        switch (value) {
            case 'urgent':
                $('#urgent-modal').modal('show')
                break;
            case 'probable':
                $('#probable-modal').modal('show')
                break;
            default:
                break;
        }
    })
})