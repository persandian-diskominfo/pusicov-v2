$(function() {
    const baseUrl = $('base#base-url').attr('href')
    const currentUrl = document.location.href
    let lastSegment = currentUrl.substring(currentUrl.lastIndexOf('/') + 1).split('#')[0]
    const strWilayah = lastSegment === 'peta-persebaran' ? 'bandung' : lastSegment.replace('%20', '-').toLowerCase()
    let url = strWilayah === 'bandung'
    ? `${baseUrl}/ajax/peta-kasus`
    : `${baseUrl}/ajax/peta-kasus/${strWilayah}`

    $.get(url, result => {
        result = JSON.parse(result)
        const wilayah = result.wilayah
        const total = result.total
        const aktif = result.aktif
        const sembuh = result.sembuh
        const meninggal = result.meninggal

        $('#map').vectorMap({
            map: strWilayah,
            zoomOnScroll: false,
            backgroundColor: 'white',
            regionStyle: {
                initial: {
                    fill: 'rgba(0, 0, 0, .1)',
                    stroke: 'white',
                    'stroke-width': 2
                },
                hover: {
                    'fill-opacity': .8
                }
            },
            series: {
                regions: [{
                    scale: ['#DEEBF7', '#08519C'],
                    values: aktif,
                    attribute: 'fill'
                }],
            },
            labels: {
                regions: {
                    render: code => wilayah[code]
                },
            },
            regionLabelStyle: {
                initial: {
                    fill: 'white',
                    stroke: 'black',
                    'stroke-width': .6,
                    'font-size': 12,
                    'font-weight': 'bold',
                    'font-family': 'Poppins'
                }
            },
            onRegionTipShow: (e, tip, code) => {
                tip.html(`
                    <div style="font-family: 'Poppins'; letter-spacing: .5px; padding: 4px 8px;">
                        <b style="font-size: 18px">${wilayah[code]}</b><br>
                        Total Kasus Terkonfirmasi: ${total[code]}<br>
                        Terkonfirmasi Aktif: ${aktif[code]}<br>
                        Terkonfirmasi Sembuh: ${sembuh[code]}<br>
                        Terkonfirmasi Meninggal: ${meninggal[code]}
                    </div>
                `)
            },
            onRegionClick: (e, code) => {
                strWilayah === 'bandung' ? document.location = `${currentUrl}/${code.replace(' ', '-').toLowerCase()}` : null
            }
        })
    })

    url = strWilayah === 'bandung'
    ? `${baseUrl}/ajax/cases`
    : `${baseUrl}/ajax/cases/${strWilayah}`
    $.get(`${url}`, result => {
        Chart.defaults.set('plugins.datalabels', {
            color: '#fff'
        })
        result = JSON.parse(result)
        let dataset = {
            labels: result.case.labels,
            datasets: [{
                label: 'Terkonfirmasi Aktif',
                data: result.case.data,
                borderColor: '#dc3545',
                backgroundColor: '#dc3545',
            }],
        }
        let delayed
        let config = {
            type: 'bar',
            data: dataset,
            options: {
                indexAxis: 'y',
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                maintainAspectRatio: false,
                responsive: true,
                animation: {
                    onComplete: () => {
                        delayed = true
                    },
                    delay: context => {
                        let delay = 0
                        if (context.type === 'data' && context.mode === 'default' && !delayed) {
                            delay = context.dataIndex * 2 + context.datasetIndex * 2
                        }

                        return delay
                    }
                },
                plugins: {
                    legend: {
                        display: false
                    }
                }
            },
            plugins: [ChartDataLabels]
        }
        
        const ctxKasus = $('#cases')
        const chartKasus = new Chart(ctxKasus, config)
    })
})