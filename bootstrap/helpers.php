<?php

use Illuminate\Contracts\Routing\UrlGenerator;

if (! function_exists('custom_asset')) {
    /**
     * Generate an asset path for the application.
     *
     * @param  string  $path
     * @param  bool|null  $secure
     * @return string
     */
    function custom_asset($path, $secure = false)
    {
        return app('url')->asset($path, $secure);
    }
}

if (! function_exists('custom_url')) {
    /**
     * Generate a url for the application.
     *
     * @param  string|null  $path
     * @param  mixed  $parameters
     * @param  bool|null  $secure
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    function custom_url($path = null, $parameters = [], $secure = false)
    {
        if (is_null($path)) {
            return app(UrlGenerator::class);
        }

        return app(UrlGenerator::class)->to($path, $parameters, $secure);
    }
}