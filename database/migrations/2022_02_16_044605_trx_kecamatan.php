<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_kecamatan', function(Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('kecamatan');
            $table->bigInteger('total_terkonfirmasi');
            $table->bigInteger('terkonfirmasi_aktif');
            $table->bigInteger('terkonfirmasi_sembuh');
            $table->bigInteger('terkonfirmasi_meninggal');
            $table->bigInteger('total_suspek');
            $table->bigInteger('suspek_dalam_pantauan');
            $table->bigInteger('suspek_discarded');
            $table->bigInteger('total_kontak_erat');
            $table->bigInteger('kontak_erat_dalam_pantauan');
            $table->bigInteger('kontak_erat_discarded');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_kecamatan');
    }
};
