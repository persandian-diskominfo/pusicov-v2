<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_vaksinasi', function(Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->tinyInteger('dosis_vaksin');
            $table->bigInteger('sasaran_nakes');
            $table->bigInteger('sasaran_petugas_publik');
            $table->bigInteger('sasaran_lansia');
            $table->bigInteger('sasaran_masyarakat');
            $table->bigInteger('sasaran_remaja');
            $table->bigInteger('sasaran_gotong_royong');
            $table->bigInteger('sasaran_anak');
            $table->bigInteger('sasaran_baru');
            $table->bigInteger('vaksin_nakes');
            $table->bigInteger('vaksin_lansia');
            $table->bigInteger('vaksin_petugas_publik');
            $table->bigInteger('vaksin_masyarakat');
            $table->bigInteger('vaksin_remaja');
            $table->bigInteger('vaksin_anak');
            $table->bigInteger('vaksin_baru');
            $table->bigInteger('vaksin_gotong_royong');
            $table->tinyInteger('vaksin_uji_klinis');
            $table->bigInteger('vaksin_ibu_hamil');
            $table->bigInteger('vaksin_disabilitas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_vaksinasi');
    }
};
