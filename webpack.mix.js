const mix = require('laravel-mix');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/theme.js', 'public/assets/js')
    .postCss('resources/assets/css/bootstrap.min.css', 'public/assets/css')
    .postCss('resources/assets/css/responsive.css', 'public/assets/css')
    .postCss('resources/assets/css/style.css', 'public/assets/css')
    .purgeCss({enabled: true});
