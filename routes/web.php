<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BaseController::class, 'index']);
Route::get('/detail-kasus', [BaseController::class, 'detail_kasus']);
Route::get('/peta-persebaran', [BaseController::class, 'peta']);
Route::get('/peta-persebaran/{kecamatan}', [BaseController::class, 'peta']);
Route::get('/faq', function() {
    return view('faq');
});
Route::get('/vaksinasi', [BaseController::class, 'detail_vaksin']);
Route::get('/help', function() {
    return view('help');
});
Route::get('cek-mandiri', [BaseController::class, 'self_checkup']);

Route::prefix('ajax')->group(function() {
    Route::get('chart-summary', [BaseController::class, 'chart_summary']);
    Route::get('peta-kasus', [BaseController::class, 'fetch_peta']);
    Route::get('peta-kasus/{kecamatan}', [BaseController::class, 'fetch_peta']);
    Route::get('cases', [BaseController::class, 'cases']);
    Route::get('cases/{kecamatan}', [BaseController::class, 'cases']);
    Route::get('vaksin', [BaseController::class, 'vaksin']);
});

Route::get('/login', function() {
    return view('login');
})->name('login');
Route::post('/authenticate', [AuthController::class, 'authenticate'])->name('auth');
Route::middleware(['auth'])->group(function() {
    Route::get('/dashboard', [AdminController::class, 'index']);
    Route::post('/fetch/{source}', [AdminController::class, 'fetch']);
    Route::get('/logout', [AuthController::class, 'logout']);
});

Route::get('/daily/update', [AdminController::class, 'cron']);
Route::get('/daily/update/{date}', [AdminController::class, 'cron_daily']);
