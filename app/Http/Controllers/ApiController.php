<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kota;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Vaksin;
use App\Models\FAQ;

class ApiController extends Controller
{
    function case_summary() {
        $latest_date = Kota::max('date');
        $latest_updates = Kota::where('date', $latest_date)->first();
        $previous_day = Kota::select('date')->orderByDesc('date')->groupBy('date')->skip(1)->take(1)->first()['date'];
        $previous_updates = Kota::where('date', $previous_day)->first();

        $date = $this->format_date($latest_date);
        $latest_updates['date'] = $date;

        return response()->json(['data' => [
            'latest_updates' => $latest_updates,
            'selisih' => [
                'total_terkonfirmasi' => $latest_updates['total_terkonfirmasi'] - $previous_updates['total_terkonfirmasi'],
                'terkonfirmasi_aktif' => $latest_updates['terkonfirmasi_aktif'] - $previous_updates['terkonfirmasi_aktif'],
                'terkonfirmasi_sembuh' => $latest_updates['terkonfirmasi_sembuh'] - $previous_updates['terkonfirmasi_sembuh'],
                'terkonfirmasi_meninggal' => $latest_updates['terkonfirmasi_meninggal'] - $previous_updates['terkonfirmasi_meninggal'],
                'total_suspek' => $latest_updates['total_suspek'] - $previous_updates['total_suspek'],
                'suspek_dalam_pantauan' => $latest_updates['suspek_dalam_pantauan'] - $previous_updates['suspek_dalam_pantauan'],
                'suspek_discarded' => $latest_updates['suspek_discarded'] - $previous_updates['suspek_discarded'],
                'total_kontak_erat' => $latest_updates['total_kontak_erat'] - $previous_updates['total_kontak_erat'],
                'kontak_erat_dalam_pantauan' => $latest_updates['kontak_erat_dalam_pantauan'] - $previous_updates['kontak_erat_dalam_pantauan'],
                'kontak_erat_discarded' => $latest_updates['kontak_erat_discarded'] - $previous_updates['kontak_erat_discarded'],
            ]
        ]]);
    }

    function case($kecamatan = null) {
        $latest_date = Kota::max('date');
        $formatted_date = $this->format_date($latest_date);
        $result = [];

        if ($kecamatan === null) {
            $result = Kecamatan::where('date', $latest_date)->get()->toArray();
        } else {
            $result = Kelurahan::where('date', $latest_date)->where('kecamatan', $kecamatan)->get()->toArray();
        }

        for ($i = 0; $i < count($result); $i++) {
            $result[$i]['date'] = $formatted_date;
        }

        return response()->json(['data' => $result]);
    }

    function vaccination() {
        $vaksin = Vaksin::where('date', Vaksin::max('date'))->get()->toArray();
        $vaksin1 = array_filter($vaksin, function($val) {
            return $val['dosis_vaksin'] == 1;
        });
        $vaksin2 = array_filter($vaksin, function($val) {
            return $val['dosis_vaksin'] == 2;
        });
        $vaksin3 = array_filter($vaksin, function($val) {
            return $val['dosis_vaksin'] == 3;
        });

        $vaksin1 = $vaksin1[array_key_first($vaksin1)];
        $vaksin2 = $vaksin2[array_key_first($vaksin2)];
        $vaksin3 = $vaksin3[array_key_first($vaksin3)];
        $vaksin = [
            'date' => $this->format_date(Vaksin::max('date')),
            'sasaran_nakes' => $vaksin1['sasaran_nakes'],
            'sasaran_petugas_publik' => $vaksin1['sasaran_petugas_publik'],
            'sasaran_lansia' => $vaksin1['sasaran_lansia'],
            'sasaran_masyarakat' => $vaksin1['sasaran_masyarakat'],
            'sasaran_remaja' => $vaksin1['sasaran_remaja'],
            'sasaran_gotong_royong' => $vaksin1['sasaran_gotong_royong'],
            'sasaran_anak' => $vaksin1['sasaran_anak'],
            'sasaran_baru' => $vaksin1['sasaran_baru'],
            'vaksin_1' => [
                'tenaga_kesehatan' => $vaksin1['vaksin_nakes'],
                'petugas_publik' => $vaksin1['vaksin_petugas_publik'],
                'lansia' => $vaksin1['vaksin_lansia'],
                'masyarakat' => $vaksin1['vaksin_masyarakat'],
                'remaja' => $vaksin1['vaksin_remaja'],
                'anak' => $vaksin1['vaksin_anak'],
            ],
            'vaksin_2' => [
                'tenaga_kesehatan' => $vaksin2['vaksin_nakes'],
                'petugas_publik' => $vaksin2['vaksin_petugas_publik'],
                'lansia' => $vaksin2['vaksin_lansia'],
                'masyarakat' => $vaksin2['vaksin_masyarakat'],
                'remaja' => $vaksin2['vaksin_remaja'],
                'anak' => $vaksin2['vaksin_anak'],
            ],
            'vaksin_3' => [
                'tenaga_kesehatan' => $vaksin3['vaksin_nakes'],
                'petugas_publik' => $vaksin3['vaksin_petugas_publik'],
                'lansia' => $vaksin3['vaksin_lansia'],
                'masyarakat' => $vaksin3['vaksin_masyarakat'],
                'remaja' => $vaksin3['vaksin_remaja'],
                'anak' => $vaksin3['vaksin_anak'],
            ]
        ];

        return response()->json(['data' => $vaksin]);
    }

    function faq() {
        $faq = FAQ::get()->toArray();

        return response()->json(['data' => $faq]);
    }

    function format_date($params) {
        $date = date_create($params);
        $formatted_date = date_format($date, 'c');

        return $formatted_date;
    }
}
