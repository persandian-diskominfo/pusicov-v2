<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\Kota;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Vaksin;

class BaseController extends Controller
{
    function case_summary($params = null) {
        $latest_date = Kota::max('date');
        $latest_updates = Kota::where('date', $latest_date)->first();
        $previous_day = Kota::select('date')->orderByDesc('date')->groupBy('date')->skip(1)->take(1)->first()['date'];
        $previous_updates = Kota::where('date', $previous_day)->first();
        if ($params !== null) {
            $params = strtoupper(str_replace('-', ' ', $params));
            $latest_updates = Kecamatan::where('kecamatan', $params)->where('date', $latest_date)->first();
            $previous_updates = Kecamatan::where('kecamatan', $params)->where('date', $previous_day)->first();
        }

        $date = $this->format_date($latest_date);
        $latest_updates['date'] = $date;

        return [
            'latest_updates' => $latest_updates,
            'previous_updates' => $previous_updates
        ];
    }

    function chart_summary() {
        $labels = Kota::select('date')->orderBy('date', 'ASC')->get()->toArray();
        $labels = array_column($labels, 'date');

        $data = Kota::select([
            'total_terkonfirmasi',
            'terkonfirmasi_aktif',
            'terkonfirmasi_sembuh',
            'terkonfirmasi_meninggal',
            'total_suspek',
            'suspek_dalam_pantauan',
            'suspek_discarded',
            'total_kontak_erat',
            'kontak_erat_dalam_pantauan',
            'kontak_erat_discarded'
        ])->orderBy('date', 'ASC')->get()->toArray();
        $total_terkonfirmasi = array_column($data, 'total_terkonfirmasi');
        $terkonfirmasi_aktif = array_column($data, 'terkonfirmasi_aktif');
        $terkonfirmasi_sembuh = array_column($data, 'terkonfirmasi_sembuh');
        $terkonfirmasi_meninggal = array_column($data, 'terkonfirmasi_meninggal');
        $total_suspek = array_column($data, 'total_suspek');
        $suspek_dalam_pantauan = array_column($data, 'suspek_dalam_pantauan');
        $suspek_discarded = array_column($data, 'suspek_discarded');
        $total_kontak_erat = array_column($data, 'total_kontak_erat');
        $kontak_erat_dalam_pantauan = array_column($data, 'kontak_erat_dalam_pantauan');
        $kontak_erat_discarded = array_column($data, 'kontak_erat_discarded');

        echo json_encode([
            'labels' => $labels,
            'data' => [
                'total_terkonfirmasi' => $total_terkonfirmasi,
                'terkonfirmasi_aktif' => $terkonfirmasi_aktif,
                'terkonfirmasi_sembuh' => $terkonfirmasi_sembuh,
                'terkonfirmasi_meninggal' => $terkonfirmasi_meninggal,
                'total_suspek' => $total_suspek,
                'suspek_dalam_pantauan' => $suspek_dalam_pantauan,
                'suspek_discarded' => $suspek_discarded,
                'total_kontak_erat' => $total_kontak_erat,
                'kontak_erat_dalam_pantauan' => $kontak_erat_dalam_pantauan,
                'kontak_erat_discarded' => $kontak_erat_discarded,
            ]
        ]);
    }

    function index() {
        $case_summary = $this->case_summary();
        extract($case_summary);

        return view('home', compact('latest_updates', 'previous_updates'));
    }

    function detail_kasus() {
        $case_summary = $this->case_summary();
        extract($case_summary);

        return view('detail_kasus', compact('latest_updates', 'previous_updates'));
    }

    function peta($kecamatan = null) {
        $latest_date = Kota::max('date');
        $latest_date = $this->format_date($latest_date);
        $kecamatan = $kecamatan !== null ? ucwords(strtolower(str_replace('-', ' ', $kecamatan))) : null;
        extract($this->case_summary($kecamatan));

        return view('peta_persebaran', compact('latest_date', 'kecamatan', 'latest_updates', 'previous_updates'));
    }

    function cases($params = null) {
        $latest_date = Kota::max('date');
        $kelurahan;
        if ($params === null) {
            $kecamatan = Kecamatan::select(['kecamatan', 'terkonfirmasi_aktif'])->where('date', $latest_date)->orderByDesc('terkonfirmasi_aktif')->get()->toArray();
            $kelurahan = Kelurahan::select(['kelurahan', 'terkonfirmasi_aktif'])->where('date', $latest_date)->orderByDesc('terkonfirmasi_aktif')->get()->toArray();
            $kecamatan = [
                'labels' => array_map([$this, 'camel_case'], array_column($kecamatan, 'kecamatan')),
                'data' => array_column($kecamatan, 'terkonfirmasi_aktif')
            ];
        } else {
            $params = strtoupper(str_replace('-', ' ', $params));
            $kelurahan = Kelurahan::select(['kecamatan', 'kelurahan', 'terkonfirmasi_aktif'])->where('kecamatan', $params)->where('date', $latest_date)->orderByDesc('terkonfirmasi_aktif')->get()->toArray();
        }

        $kelurahan = [
            'labels' => array_column($kelurahan, 'kelurahan'),
            'data' => array_column($kelurahan, 'terkonfirmasi_aktif')
        ];
            
        echo json_encode(
            $params === null
            ? [
                'case' => $kecamatan,
                'kelurahan' => $kelurahan
            ]
            : [
                'case' => $kelurahan
            ]
        );
    }

    function fetch_peta($kecamatan = null) {
        $latest_date = Kota::max('date');
        if ($kecamatan == null) {
            $wilayah = Kecamatan::where('date', $latest_date)->get()->toArray();
        } else {
            $kecamatan = strtoupper(str_replace('-', ' ', $kecamatan));
            $wilayah = Kelurahan::where('kecamatan', strtoupper($kecamatan))->where('date', $latest_date)->get()->toArray();
        }

        $total = array_column($wilayah, 'total_terkonfirmasi');
        $aktif = array_column($wilayah, 'terkonfirmasi_aktif');
        $sembuh = array_column($wilayah, 'terkonfirmasi_sembuh');
        $meninggal = array_column($wilayah, 'terkonfirmasi_meninggal');
        $wilayah = array_column($wilayah, $kecamatan === null ? 'kecamatan' : 'kelurahan');

        $total = array_combine(array_map('strtoupper', $wilayah), array_map('number_format', $total));
        $aktif = array_combine(array_map('strtoupper', $wilayah), array_map('number_format', $aktif));
        $sembuh = array_combine(array_map('strtoupper', $wilayah), array_map('number_format', $sembuh));
        $meninggal = array_combine(array_map('strtoupper', $wilayah), array_map('number_format', $meninggal));
        $wilayah = array_combine(array_map('strtoupper', $wilayah), array_map([$this, 'camel_case'], $wilayah));

        echo json_encode([
            "wilayah" => $wilayah,
            "total" => $total,
            "aktif" => $aktif,
            "sembuh" => $sembuh,
            "meninggal" => $meninggal
        ]);
    }

    function detail_vaksin() {
        $date = $this->format_date(Vaksin::max('date'));
        $vaksin = $this->get_vaksin();

        return view('vaksin', compact('date', 'vaksin'));
    }

    function vaksin() {
        $vaksin = $this->get_vaksin();
        $vaksin['vaksin_1'] = [
            'labels' => array_keys($vaksin['vaksin_1']),
            'data' => array_values($vaksin['vaksin_1'])
        ];
        $vaksin['vaksin_2'] = [
            'labels' => array_keys($vaksin['vaksin_2']),
            'data' => array_values($vaksin['vaksin_2'])
        ];
        $vaksin['vaksin_3'] = [
            'labels' => array_keys($vaksin['vaksin_3']),
            'data' => array_values($vaksin['vaksin_3'])
        ];
        
        echo json_encode($vaksin);
    }

    function get_vaksin() {
        $vaksin = Vaksin::where('date', Vaksin::max('date'))->get()->toArray();
        $vaksin1 = array_filter($vaksin, function($val) {
            return $val['dosis_vaksin'] == 1;
        });
        $vaksin2 = array_filter($vaksin, function($val) {
            return $val['dosis_vaksin'] == 2;
        });
        $vaksin3 = array_filter($vaksin, function($val) {
            return $val['dosis_vaksin'] == 3;
        });

        $vaksin1 = $vaksin1[array_key_first($vaksin1)];
        $vaksin2 = $vaksin2[array_key_first($vaksin2)];
        $vaksin3 = $vaksin3[array_key_first($vaksin3)];
        $vaksin = [
            'sasaran_nakes' => $vaksin1['sasaran_nakes'],
            'sasaran_petugas_publik' => $vaksin1['sasaran_petugas_publik'],
            'sasaran_lansia' => $vaksin1['sasaran_lansia'],
            'sasaran_masyarakat' => $vaksin1['sasaran_masyarakat'],
            'sasaran_remaja' => $vaksin1['sasaran_remaja'],
            'sasaran_gotong_royong' => $vaksin1['sasaran_gotong_royong'],
            'sasaran_anak' => $vaksin1['sasaran_anak'],
            'sasaran_baru' => $vaksin1['sasaran_baru'],
            'vaksin_1' => [
                'Tenaga Kesehatan' => $vaksin1['vaksin_nakes'],
                'Petugas Publik' => $vaksin1['vaksin_petugas_publik'],
                'Lansia' => $vaksin1['vaksin_lansia'],
                'Masyarakat' => $vaksin1['vaksin_masyarakat'],
                'Remaja' => $vaksin1['vaksin_remaja'],
                'Anak' => $vaksin1['vaksin_anak'],
                // 'Baru' => $vaksin1['vaksin_baru'],
                // 'Vaksin Gotong Royong' => $vaksin1['vaksin_gotong_royong'],
                // 'Uji Klinis' => $vaksin1['vaksin_uji_klinis'],
                // 'Ibu Hamil' => $vaksin1['vaksin_ibu_hamil'],
                // 'Disabilitas' => $vaksin1['vaksin_disabilitas'],
            ],
            'vaksin_2' => [
                'Tenaga Kesehatan' => $vaksin2['vaksin_nakes'],
                'Petugas Publik' => $vaksin2['vaksin_petugas_publik'],
                'Lansia' => $vaksin2['vaksin_lansia'],
                'Masyarakat' => $vaksin2['vaksin_masyarakat'],
                'Remaja' => $vaksin2['vaksin_remaja'],
                'Anak' => $vaksin2['vaksin_anak'],
                // 'Baru' => $vaksin2['vaksin_baru'],
                // 'Vaksin Gotong Royong' => $vaksin2['vaksin_gotong_royong'],
                // 'Uji Klinis' => $vaksin2['vaksin_uji_klinis'],
                // 'Ibu Hamil' => $vaksin2['vaksin_ibu_hamil'],
                // 'Disabilitas' => $vaksin2['vaksin_disabilitas'],
            ],
            'vaksin_3' => [
                'Tenaga Kesehatan' => $vaksin3['vaksin_nakes'],
                'Petugas Publik' => $vaksin3['vaksin_petugas_publik'],
                'Lansia' => $vaksin3['vaksin_lansia'],
                'Masyarakat' => $vaksin3['vaksin_masyarakat'],
                'Remaja' => $vaksin3['vaksin_remaja'],
                'Anak' => $vaksin3['vaksin_anak'],
                // 'Baru' => $vaksin3['vaksin_baru'],
                // 'Vaksin Gotong Royong' => $vaksin3['vaksin_gotong_royong'],
                // 'Uji Klinis' => $vaksin3['vaksin_uji_klinis'],
                // 'Ibu Hamil' => $vaksin3['vaksin_ibu_hamil'],
                // 'Disabilitas' => $vaksin3['vaksin_disabilitas'],
            ]
        ];

        return $vaksin;
    }

    function self_checkup() {
        return view('self_checkup');
    }

    function camel_case($value) {
        return ucwords(strtolower($value));
    }

    function format_date($params) {
        $months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $date = date_create($params);
        $day = date_format($date, "d");
        $month = $months[date_format($date, "n") - 1];
        $year = date_format($date, "Y");

        return "$day $month $year";
    }
}
