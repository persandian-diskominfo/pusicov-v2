<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Maatwebsite\Excel\Facades\Excel;

use App\Imports\KelurahanImport;
use App\Models\Kota;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Vaksin;

class AdminController extends Controller
{
    private $token;

    function index() {
        $latest_date = Kota::select('date')->orderByDesc('date')->groupBy('date')->first()['date'];
        $latest_updates = Kota::where('date', $latest_date)->first();
        $previous_day = Kota::select('date')->orderByDesc('date')->groupBy('date')->skip(1)->take(1)->first()['date'];
        $previous_updates = Kota::where('date', $previous_day)->first();

        $months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $date = date_create($latest_date);
        $day = date_format($date, "d");
        $month = $months[date_format($date, "n") - 1];
        $year = date_format($date, "Y");
        $latest_updates['date'] = "$day $month $year";

        return view('dashboard.home', compact('latest_updates', 'previous_updates'));
    }

    function fetch(Request $req, $source) {
        switch ($source) {
            case 'splp':
                if ($this->generate_splp_token()) {
                    if ($this->fetch_splp()) return redirect('/dashboard')->with(['success' => 'Data from SPLP imported successfully']);
                }

                break;
            case 'mantra':
                if ($this->fetch_mantra()) return redirect('/dashboard')->with(['success' => 'Data from mantra.bandung.go.id imported successfully.']);

                break;
            case 'data-bandung':
                if ($this->fetch_data_bandung()) return redirect('/dashboard')->with(['success' => 'Data from data.bandung.go.id imported successfully.']);

                break;
            case 'excel':
                return $this->import_excel($req);

                break;
            case 'vaksin-splp':
                if ($this->generate_splp_token()) {
                    if ($this->fetch_vaksin_splp()) return redirect('/dashboard')->with(['success' => 'Vaccination data imported successfully']);
                }

                break;
            case 'vaksin-mantra':
                if ($this->fetch_vaksin_mantra()) return redirect('/dashboard')->with(['success' => 'Vaccination data imported successfully']);

                break;
            case 'vaksin-data-bandung':
                if ($this->fetch_vaksin_data_bandung()) return redirect('/dashboard')->with(['success' => 'Vaccination data imported successfully']);

                break;
            default:
                return redirect('/dashboard');
                
                break;
        }
    }

    function fetch_vaksin_mantra() {
        $headers = [
            'User-Agent' => 'MANTRA',
            'AccessKey' => env('MANTRA_VAKSIN_TERKINI_ACCESS_KEY')
        ];
        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://mantra.bandung.go.id/mantra/json/diskominfo/data/vaksinasi_terkini');
        $res = $response->json()['response']['data'];
        $latest_date = $res['date'];
        $existing = Vaksin::orderByDesc('id')->first()['date'];
        
        if ($latest_date == $existing) return back()->with(['error' => 'Tidak ada data baru yang dapat diperbarui.']);

        $dosis = [
            'date' => $latest_date,
            'sasaran_nakes' => $res['sasaran_nakes'],
            'sasaran_petugas_publik' => $res['sasaran_petugas_publik'],
            'sasaran_lansia' => $res['sasaran_lansia'],
            'sasaran_masyarakat' => $res['sasaran_masyarakat'],
            'sasaran_remaja' => $res['sasaran_remaja'],
            'sasaran_gotong_royong' => $res['sasaran_gotong_royong'],
            'sasaran_anak' => $res['sasaran_anak'],
            'sasaran_baru' => 0,
        ];
        $dosis_1 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][0]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][0]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][0]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][0]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][0]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][0]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][0]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][0]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][0]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][0]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][0]['vaksin_disabilitas'],
        ];
        $dosis_2 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][1]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][1]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][1]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][1]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][1]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][1]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][1]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][1]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][1]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][1]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][1]['vaksin_disabilitas'],
        ];
        $dosis_3 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][2]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][2]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][2]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][2]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][2]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][2]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][2]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][2]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][2]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][2]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][2]['vaksin_disabilitas'],
        ];

        $insert = Vaksin::upsert([
            $dosis_1,
            $dosis_2,
            $dosis_3
        ], []);

        if (!$insert) return back()->with(['error' => 'Failed to import summary data.']);

        return true;
    }

    function fetch_vaksin_data_bandung() {
        $response = Http::connectTimeout(20)->retry(3, 200)->get('http://data.bandung.go.id/service/index.php/vaksinasi/terkini');
        $res = $response->json();
        $latest_date = $res['date'];
        $existing = Vaksin::orderByDesc('id')->first()['date'];
        
        if ($latest_date == $existing) return back()->with(['error' => 'Tidak ada data baru yang dapat diperbarui.']);

        $dosis = [
            'date' => $latest_date,
            'sasaran_nakes' => $res['sasaran_nakes'],
            'sasaran_petugas_publik' => $res['sasaran_petugas_publik'],
            'sasaran_lansia' => $res['sasaran_lansia'],
            'sasaran_masyarakat' => $res['sasaran_masyarakat'],
            'sasaran_remaja' => $res['sasaran_remaja'],
            'sasaran_gotong_royong' => $res['sasaran_gotong_royong'],
            'sasaran_anak' => $res['sasaran_anak'],
            'sasaran_baru' => 0,
        ];
        $dosis_1 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][0]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][0]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][0]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][0]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][0]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][0]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][0]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][0]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][0]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][0]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][0]['vaksin_disabilitas'],
        ];
        $dosis_2 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][1]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][1]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][1]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][1]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][1]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][1]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][1]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][1]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][1]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][1]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][1]['vaksin_disabilitas'],
        ];
        $dosis_3 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][2]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][2]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][2]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][2]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][2]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][2]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][2]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][2]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][2]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][2]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][2]['vaksin_disabilitas'],
        ];

        $insert = Vaksin::upsert([
            $dosis_1,
            $dosis_2,
            $dosis_3
        ], []);

        if (!$insert) return back()->with(['error' => 'Failed to import summary data.']);

        return true;
    }

    function fetch_mantra() {
        $headers = [
            'User-Agent' => 'MANTRA',
            'AccessKey' => env('MANTRA_COVID_TERKINI_ACCESS_KEY')
        ];
        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://mantra.bandung.go.id/mantra/json/diskominfo/data/covid_terkini/area=kota');
        $result = $response->json()['response']['data']['data'][0];
        $latest_mantra = $result['date'];
        $existing = Kota::orderByDesc('id')->first()['date'];
        
        if ($latest_mantra == $existing) return back()->with(['error' => 'Tidak ada data baru yang dapat diperbarui.']);

        $data_kota = [
            'date' => $latest_mantra,
            'total_terkonfirmasi' => $result['konfirmasi']['jumlah_konfirmasi'],
            'terkonfirmasi_aktif' => $result['konfirmasi']['konfirmasi_aktif'],
            'terkonfirmasi_sembuh' => $result['konfirmasi']['konfirmasi_sembuh'],
            'terkonfirmasi_meninggal' => $result['konfirmasi']['konfirmasi_meninggal'],
            'total_suspek' => $result['suspek']['jumlah_suspek'],
            'suspek_dalam_pantauan' => $result['suspek']['suspek_dipantau'],
            'suspek_discarded' => $result['suspek']['suspek_discarded'],
            'total_kontak_erat' => $result['kontak_erat']['jumlah_kontak_erat'],
            'kontak_erat_dalam_pantauan' => $result['kontak_erat']['kontak_erat_dipantau'],
            'kontak_erat_discarded' => $result['kontak_erat']['kontak_erat_discarded']
        ];
        $kota = Kota::create($data_kota);

        if (!$kota) return back()->with(['error' => 'Failed to import summary data.']);

        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://mantra.bandung.go.id/mantra/json/diskominfo/data/covid_terkini/area=kecamatan');
        $result = $response->json()['response']['data']['data'];

        $data_kecamatan = [];
        foreach($result as $res) {
            $data_kecamatan[] = [
                'date' => $res['date'],
                'kecamatan' => $res['kecamatan'],
                'total_terkonfirmasi' => $res['konfirmasi']['jumlah_konfirmasi'],
                'terkonfirmasi_aktif' => $res['konfirmasi']['konfirmasi_aktif'],
                'terkonfirmasi_sembuh' => $res['konfirmasi']['konfirmasi_sembuh'],
                'terkonfirmasi_meninggal' => $res['konfirmasi']['konfirmasi_meninggal'],
                'total_suspek' => $res['suspek']['jumlah_suspek'],
                'suspek_dalam_pantauan' => $res['suspek']['suspek_dipantau'],
                'suspek_discarded' => $res['suspek']['suspek_discarded'],
                'total_kontak_erat' => $res['kontak_erat']['jumlah_kontak_erat'],
                'kontak_erat_dalam_pantauan' => $res['kontak_erat']['kontak_erat_dipantau'],
                'kontak_erat_discarded' => $res['kontak_erat']['kontak_erat_discarded']
            ];
        }
        $kecamatan = Kecamatan::upsert($data_kecamatan, []);

        if (!$kecamatan) return back()->with(['error' => 'Failed to import data kecamatan.']);

        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://mantra.bandung.go.id/mantra/json/diskominfo/data/covid_terkini/area=kelurahan');
        $result = $response->json()['response']['data']['data'];

        $data_kelurahan = [];
        foreach($result as $res) {
            $data_kelurahan[] = [
                'date' => $res['date'],
                'kecamatan' => $res['kecamatan'],
                'kelurahan' => $res['kelurahan'],
                'total_terkonfirmasi' => $res['konfirmasi']['jumlah_konfirmasi'],
                'terkonfirmasi_aktif' => $res['konfirmasi']['konfirmasi_aktif'],
                'terkonfirmasi_sembuh' => $res['konfirmasi']['konfirmasi_sembuh'],
                'terkonfirmasi_meninggal' => $res['konfirmasi']['konfirmasi_meninggal'],
                'total_suspek' => $res['suspek']['jumlah_suspek'],
                'suspek_dalam_pantauan' => $res['suspek']['suspek_dipantau'],
                'suspek_discarded' => $res['suspek']['suspek_discarded'],
                'total_kontak_erat' => $res['kontak_erat']['jumlah_kontak_erat'],
                'kontak_erat_dalam_pantauan' => $res['kontak_erat']['kontak_erat_dipantau'],
                'kontak_erat_discarded' => $res['kontak_erat']['kontak_erat_discarded']
            ];
        }
        $kelurahan = Kelurahan::upsert($data_kelurahan, []);

        if (!$kelurahan) return back()->with(['error' => 'Failed to import data kelurahan.']);

        return true;
    }

    function fetch_data_bandung() {
        $response = Http::connectTimeout(20)->retry(3, 200)->get('http://data.bandung.go.id/service/index.php/covid/terkini/kota');
        $result = $response->json()['data'][0];
        $latest_mantra = $result['date'];
        $existing = Kota::orderByDesc('id')->first()['date'];
        
        if ($latest_mantra == $existing) return back()->with(['error' => 'Tidak ada data baru yang dapat diperbarui.']);

        $data_kota = [
            'date' => $latest_mantra,
            'total_terkonfirmasi' => $result['konfirmasi']['jumlah_konfirmasi'],
            'terkonfirmasi_aktif' => $result['konfirmasi']['konfirmasi_aktif'],
            'terkonfirmasi_sembuh' => $result['konfirmasi']['konfirmasi_sembuh'],
            'terkonfirmasi_meninggal' => $result['konfirmasi']['konfirmasi_meninggal'],
            'total_suspek' => $result['suspek']['jumlah_suspek'],
            'suspek_dalam_pantauan' => $result['suspek']['suspek_dipantau'],
            'suspek_discarded' => $result['suspek']['suspek_discarded'],
            'total_kontak_erat' => $result['kontak_erat']['jumlah_kontak_erat'],
            'kontak_erat_dalam_pantauan' => $result['kontak_erat']['kontak_erat_dipantau'],
            'kontak_erat_discarded' => $result['kontak_erat']['kontak_erat_discarded']
        ];
        $kota = Kota::create($data_kota);

        if (!$kota) return back()->with(['error' => 'Failed to import summary data.']);

        $response = Http::connectTimeout(20)->retry(3, 200)->get('http://data.bandung.go.id/service/index.php/covid/terkini/kecamatan');
        $result = $response->json()['data'];

        $data_kecamatan = [];
        foreach($result as $res) {
            $data_kecamatan[] = [
                'date' => $res['date'],
                'kecamatan' => $res['kecamatan'],
                'total_terkonfirmasi' => $res['konfirmasi']['jumlah_konfirmasi'],
                'terkonfirmasi_aktif' => $res['konfirmasi']['konfirmasi_aktif'],
                'terkonfirmasi_sembuh' => $res['konfirmasi']['konfirmasi_sembuh'],
                'terkonfirmasi_meninggal' => $res['konfirmasi']['konfirmasi_meninggal'],
                'total_suspek' => $res['suspek']['jumlah_suspek'],
                'suspek_dalam_pantauan' => $res['suspek']['suspek_dipantau'],
                'suspek_discarded' => $res['suspek']['suspek_discarded'],
                'total_kontak_erat' => $res['kontak_erat']['jumlah_kontak_erat'],
                'kontak_erat_dalam_pantauan' => $res['kontak_erat']['kontak_erat_dipantau'],
                'kontak_erat_discarded' => $res['kontak_erat']['kontak_erat_discarded']
            ];
        }
        $kecamatan = Kecamatan::upsert($data_kecamatan, []);

        if (!$kecamatan) return back()->with(['error' => 'Failed to import data kecamatan.']);

        $response = Http::connectTimeout(20)->retry(3, 200)->get('http://data.bandung.go.id/service/index.php/covid/terkini/kelurahan');
        $result = $response->json()['data'];

        $data_kelurahan = [];
        foreach($result as $res) {
            $data_kelurahan[] = [
                'date' => $res['date'],
                'kecamatan' => $res['kecamatan'],
                'kelurahan' => $res['kelurahan'],
                'total_terkonfirmasi' => $res['konfirmasi']['jumlah_konfirmasi'],
                'terkonfirmasi_aktif' => $res['konfirmasi']['konfirmasi_aktif'],
                'terkonfirmasi_sembuh' => $res['konfirmasi']['konfirmasi_sembuh'],
                'terkonfirmasi_meninggal' => $res['konfirmasi']['konfirmasi_meninggal'],
                'total_suspek' => $res['suspek']['jumlah_suspek'],
                'suspek_dalam_pantauan' => $res['suspek']['suspek_dipantau'],
                'suspek_discarded' => $res['suspek']['suspek_discarded'],
                'total_kontak_erat' => $res['kontak_erat']['jumlah_kontak_erat'],
                'kontak_erat_dalam_pantauan' => $res['kontak_erat']['kontak_erat_dipantau'],
                'kontak_erat_discarded' => $res['kontak_erat']['kontak_erat_discarded']
            ];
        }
        $kelurahan = Kelurahan::upsert($data_kelurahan, []);

        if (!$kelurahan) return back()->with(['error' => 'Failed to import data kelurahan.']);

        return true;
    }

    function import_excel($req) {
        $validation = $req->validate([
            'file' => 'required|mimes:xlsx|max:2048'
        ]);
        if (!$validation) return back()->withErrors(['file' => $validation->errors()]);

        $kelurahan = Excel::toArray(new KelurahanImport, $req->file)[0];
        $kecamatan = [];
        foreach($kelurahan as $kel) {
            $found_id = array_search($kel[1], array_column($kecamatan, 'kecamatan'));
            if ($found_id !== FALSE) {
                $kecamatan[$found_id]['total_terkonfirmasi'] += $kel[3];          // If you saw this code and getting confused, don't worry. IT CONFUSES TOO.
                $kecamatan[$found_id]['terkonfirmasi_aktif'] += $kel[4];
                $kecamatan[$found_id]['terkonfirmasi_sembuh'] += $kel[5];
                $kecamatan[$found_id]['terkonfirmasi_meninggal'] += $kel[6];
                $kecamatan[$found_id]['total_suspek'] += $kel[7];
                $kecamatan[$found_id]['suspek_dalam_pantauan'] += $kel[8];
                $kecamatan[$found_id]['suspek_discarded'] += $kel[9];
                $kecamatan[$found_id]['total_kontak_erat'] += $kel[10];
                $kecamatan[$found_id]['kontak_erat_dalam_pantauan'] += $kel[11];
                $kecamatan[$found_id]['kontak_erat_discarded'] += $kel[12];
            } else {
                array_push($kecamatan, [
                    'date' => $kel[0],
                    'kecamatan' => $kel[1],
                    'total_terkonfirmasi' => $kel[3],
                    'terkonfirmasi_aktif' => $kel[4],
                    'terkonfirmasi_sembuh' => $kel[5],
                    'terkonfirmasi_meninggal' => $kel[6],
                    'total_suspek' => $kel[7],
                    'suspek_dalam_pantauan' => $kel[8],
                    'suspek_discarded' => $kel[9],
                    'total_kontak_erat' => $kel[10],
                    'kontak_erat_dalam_pantauan' => $kel[11],
                    'kontak_erat_discarded' => $kel[12]
                ]);
            }
        }

        $kota = [
            'date' => $kecamatan[0]['date'],
            'total_terkonfirmasi' => array_sum(array_column($kecamatan, 'total_terkonfirmasi')),
            'terkonfirmasi_aktif' => array_sum(array_column($kecamatan, 'terkonfirmasi_aktif')),
            'terkonfirmasi_sembuh' => array_sum(array_column($kecamatan, 'terkonfirmasi_sembuh')),
            'terkonfirmasi_meninggal' => array_sum(array_column($kecamatan, 'terkonfirmasi_meninggal')),
            'total_suspek' => array_sum(array_column($kecamatan, 'total_suspek')),
            'suspek_dalam_pantauan' => array_sum(array_column($kecamatan, 'suspek_dalam_pantauan')),
            'suspek_discarded' => array_sum(array_column($kecamatan, 'suspek_discarded')),
            'total_kontak_erat' => array_sum(array_column($kecamatan, 'total_kontak_erat')),
            'kontak_erat_dalam_pantauan' => array_sum(array_column($kecamatan, 'kontak_erat_dalam_pantauan')),
            'kontak_erat_discarded' => array_sum(array_column($kecamatan, 'kontak_erat_discarded'))
        ];

        $import_kelurahan = Excel::import(new KelurahanImport, $req->file);
        if (!$import_kelurahan) return back()->with(['error' => 'Failed to import to table kelurahan']);

        $import_kecamatan = Kecamatan::upsert($kecamatan, []);
        if (!$import_kecamatan) return back()->with(['error' => 'Failed to import to table kecamatan']);

        $import_kota = Kota::create($kota);
        if (!$import_kota) return back()->with(['error' => 'Failed to import to table kota']);

        return redirect('/dashboard')->with(['success' => 'Data from excel file imported successfully.']);
    }

    function cron() {
        if ($this->generate_splp_token()) {
            if ($this->fetch_splp() && $this->fetch_vaksin_splp()) {
                return true;
            }

            // $this->cron();
        }

        return false;
    }
    
    function cron_daily($date) {
        if ($this->fetch_mantra_daily($date) && $this->fetch_vaksin_data_bandung_daily($date)) {
            return true;
        }

        // $this->cron_daily();
    }

    function fetch_mantra_daily($date) {
        $headers = [
            'User-Agent' => 'MANTRA',
            'AccessKey' => env('MANTRA_COVID_BY_DATE_ACCESS_KEY')
        ];
        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://mantra.bandung.go.id/mantra/json/diskominfo/data/covid_by_date/area=kota&tanggal=' . $date);
        $result = $response->json()['response']['data']['data'][0];
        $latest_mantra = $result['date'];
        $existing = Kota::orderByDesc('id')->first()['date'];
        
        if ($latest_mantra == $existing) return back()->with(['error' => 'Tidak ada data baru yang dapat diperbarui.']);

        $data_kota = [
            'date' => $latest_mantra,
            'total_terkonfirmasi' => $result['konfirmasi']['jumlah_konfirmasi'],
            'terkonfirmasi_aktif' => $result['konfirmasi']['konfirmasi_aktif'],
            'terkonfirmasi_sembuh' => $result['konfirmasi']['konfirmasi_sembuh'],
            'terkonfirmasi_meninggal' => $result['konfirmasi']['konfirmasi_meninggal'],
            'total_suspek' => $result['suspek']['jumlah_suspek'],
            'suspek_dalam_pantauan' => $result['suspek']['suspek_dipantau'],
            'suspek_discarded' => $result['suspek']['suspek_discarded'],
            'total_kontak_erat' => $result['kontak_erat']['jumlah_kontak_erat'],
            'kontak_erat_dalam_pantauan' => $result['kontak_erat']['kontak_erat_dipantau'],
            'kontak_erat_discarded' => $result['kontak_erat']['kontak_erat_discarded']
        ];
        $kota = Kota::create($data_kota);

        if (!$kota) return back()->with(['error' => 'Failed to import summary data.']);

        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://mantra.bandung.go.id/mantra/json/diskominfo/data/covid_by_date/area=kecamatan&tanggal=' . $date);
        $result = $response->json()['response']['data']['data'];

        $data_kecamatan = [];
        foreach($result as $res) {
            $data_kecamatan[] = [
                'date' => $res['date'],
                'kecamatan' => $res['kecamatan'],
                'total_terkonfirmasi' => $res['konfirmasi']['jumlah_konfirmasi'],
                'terkonfirmasi_aktif' => $res['konfirmasi']['konfirmasi_aktif'],
                'terkonfirmasi_sembuh' => $res['konfirmasi']['konfirmasi_sembuh'],
                'terkonfirmasi_meninggal' => $res['konfirmasi']['konfirmasi_meninggal'],
                'total_suspek' => $res['suspek']['jumlah_suspek'],
                'suspek_dalam_pantauan' => $res['suspek']['suspek_dipantau'],
                'suspek_discarded' => $res['suspek']['suspek_discarded'],
                'total_kontak_erat' => $res['kontak_erat']['jumlah_kontak_erat'],
                'kontak_erat_dalam_pantauan' => $res['kontak_erat']['kontak_erat_dipantau'],
                'kontak_erat_discarded' => $res['kontak_erat']['kontak_erat_discarded']
            ];
        }
        $kecamatan = Kecamatan::upsert($data_kecamatan, []);

        if (!$kecamatan) return back()->with(['error' => 'Failed to import data kecamatan.']);

        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://mantra.bandung.go.id/mantra/json/diskominfo/data/covid_by_date/area=kelurahan&tanggal=' . $date);
        $result = $response->json()['response']['data']['data'];

        $data_kelurahan = [];
        foreach($result as $res) {
            $data_kelurahan[] = [
                'date' => $res['date'],
                'kecamatan' => $res['kecamatan'],
                'kelurahan' => $res['kelurahan'],
                'total_terkonfirmasi' => $res['konfirmasi']['jumlah_konfirmasi'],
                'terkonfirmasi_aktif' => $res['konfirmasi']['konfirmasi_aktif'],
                'terkonfirmasi_sembuh' => $res['konfirmasi']['konfirmasi_sembuh'],
                'terkonfirmasi_meninggal' => $res['konfirmasi']['konfirmasi_meninggal'],
                'total_suspek' => $res['suspek']['jumlah_suspek'],
                'suspek_dalam_pantauan' => $res['suspek']['suspek_dipantau'],
                'suspek_discarded' => $res['suspek']['suspek_discarded'],
                'total_kontak_erat' => $res['kontak_erat']['jumlah_kontak_erat'],
                'kontak_erat_dalam_pantauan' => $res['kontak_erat']['kontak_erat_dipantau'],
                'kontak_erat_discarded' => $res['kontak_erat']['kontak_erat_discarded']
            ];
        }
        $kelurahan = Kelurahan::upsert($data_kelurahan, []);

        if (!$kelurahan) return back()->with(['error' => 'Failed to import data kelurahan.']);

        return true;
    }

    function fetch_vaksin_data_bandung_daily($date) {
        $response = Http::connectTimeout(20)->retry(3, 200)->get('http://data.bandung.go.id/service/index.php/vaksinasi/filterdate/' . $date);
        $res = $response->json();
        $latest_date = $res['date'];
        $existing = Vaksin::orderByDesc('id')->first()['date'];
        
        if ($latest_date == $existing) return back()->with(['error' => 'Tidak ada data baru yang dapat diperbarui.']);

        $dosis = [
            'date' => $latest_date,
            'sasaran_nakes' => $res['sasaran_nakes'],
            'sasaran_petugas_publik' => $res['sasaran_petugas_publik'],
            'sasaran_lansia' => $res['sasaran_lansia'],
            'sasaran_masyarakat' => $res['sasaran_masyarakat'],
            'sasaran_remaja' => $res['sasaran_remaja'],
            'sasaran_gotong_royong' => $res['sasaran_gotong_royong'],
            'sasaran_anak' => $res['sasaran_anak'],
            'sasaran_baru' => 0,
        ];
        $dosis_1 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][0]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][0]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][0]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][0]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][0]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][0]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][0]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][0]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][0]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][0]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][0]['vaksin_disabilitas'],
        ];
        $dosis_2 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][1]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][1]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][1]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][1]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][1]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][1]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][1]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][1]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][1]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][1]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][1]['vaksin_disabilitas'],
        ];
        $dosis_3 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][2]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][2]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][2]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][2]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][2]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][2]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][2]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][2]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][2]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][2]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][2]['vaksin_disabilitas'],
        ];

        $insert = Vaksin::upsert([
            $dosis_1,
            $dosis_2,
            $dosis_3
        ], []);

        if (!$insert) return back()->with(['error' => 'Failed to import summary data.']);

        return true;
    }

    public function fetch_splp()
    {
        $token = $this->token;
        $headers = [
            'Authorization' => "Bearer $token",
        ];
        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://api-splp.layanan.go.id/t/bandung.go.id/covid_terkini/1.0/kota');
        $result = $response->json()['data'][0];
        $latest_mantra = $result['date'];
        $existing = Kota::orderByDesc('id')->first()['date'];
        
        if ($latest_mantra == $existing) return back()->with(['error' => 'Tidak ada data baru yang dapat diperbarui.']);

        $data_kota = [
            'date' => $latest_mantra,
            'total_terkonfirmasi' => $result['konfirmasi']['jumlah_konfirmasi'],
            'terkonfirmasi_aktif' => $result['konfirmasi']['konfirmasi_aktif'],
            'terkonfirmasi_sembuh' => $result['konfirmasi']['konfirmasi_sembuh'],
            'terkonfirmasi_meninggal' => $result['konfirmasi']['konfirmasi_meninggal'],
            'total_suspek' => $result['suspek']['jumlah_suspek'],
            'suspek_dalam_pantauan' => $result['suspek']['suspek_dipantau'],
            'suspek_discarded' => $result['suspek']['suspek_discarded'],
            'total_kontak_erat' => $result['kontak_erat']['jumlah_kontak_erat'],
            'kontak_erat_dalam_pantauan' => $result['kontak_erat']['kontak_erat_dipantau'],
            'kontak_erat_discarded' => $result['kontak_erat']['kontak_erat_discarded']
        ];
        $kota = Kota::create($data_kota);

        if (!$kota) return back()->with(['error' => 'Failed to import summary data.']);

        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://api-splp.layanan.go.id/t/bandung.go.id/covid_terkini/1.0/kec');
        $result = $response->json()['data'];

        $data_kecamatan = [];
        foreach($result as $res) {
            $data_kecamatan[] = [
                'date' => $res['date'],
                'kecamatan' => $res['kecamatan'],
                'total_terkonfirmasi' => $res['konfirmasi']['jumlah_konfirmasi'],
                'terkonfirmasi_aktif' => $res['konfirmasi']['konfirmasi_aktif'],
                'terkonfirmasi_sembuh' => $res['konfirmasi']['konfirmasi_sembuh'],
                'terkonfirmasi_meninggal' => $res['konfirmasi']['konfirmasi_meninggal'],
                'total_suspek' => $res['suspek']['jumlah_suspek'],
                'suspek_dalam_pantauan' => $res['suspek']['suspek_dipantau'],
                'suspek_discarded' => $res['suspek']['suspek_discarded'],
                'total_kontak_erat' => $res['kontak_erat']['jumlah_kontak_erat'],
                'kontak_erat_dalam_pantauan' => $res['kontak_erat']['kontak_erat_dipantau'],
                'kontak_erat_discarded' => $res['kontak_erat']['kontak_erat_discarded']
            ];
        }
        $kecamatan = Kecamatan::upsert($data_kecamatan, []);

        if (!$kecamatan) return back()->with(['error' => 'Failed to import data kecamatan.']);

        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://api-splp.layanan.go.id/t/bandung.go.id/covid_terkini/1.0/kel');
        $result = $response->json()['data'];

        $data_kelurahan = [];
        foreach($result as $res) {
            $data_kelurahan[] = [
                'date' => $res['date'],
                'kecamatan' => $res['kecamatan'],
                'kelurahan' => $res['kelurahan'],
                'total_terkonfirmasi' => $res['konfirmasi']['jumlah_konfirmasi'],
                'terkonfirmasi_aktif' => $res['konfirmasi']['konfirmasi_aktif'],
                'terkonfirmasi_sembuh' => $res['konfirmasi']['konfirmasi_sembuh'],
                'terkonfirmasi_meninggal' => $res['konfirmasi']['konfirmasi_meninggal'],
                'total_suspek' => $res['suspek']['jumlah_suspek'],
                'suspek_dalam_pantauan' => $res['suspek']['suspek_dipantau'],
                'suspek_discarded' => $res['suspek']['suspek_discarded'],
                'total_kontak_erat' => $res['kontak_erat']['jumlah_kontak_erat'],
                'kontak_erat_dalam_pantauan' => $res['kontak_erat']['kontak_erat_dipantau'],
                'kontak_erat_discarded' => $res['kontak_erat']['kontak_erat_discarded']
            ];
        }
        $kelurahan = Kelurahan::upsert($data_kelurahan, []);

        if (!$kelurahan) return back()->with(['error' => 'Failed to import data kelurahan.']);

        return true;
    }
    
    public function fetch_vaksin_splp()
    {
        $token = $this->token;
        $headers = [
            'Authorization' => "Bearer $token",
        ];
        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->get('https://api-splp.layanan.go.id/t/bandung.go.id/covid/1.0./vaksinasi/terkini');
        $res = $response->json();
        $latest_date = $res['date'];
        $existing = Vaksin::orderByDesc('id')->first()['date'];
        
        if ($latest_date == $existing) return back()->with(['error' => 'Tidak ada data baru yang dapat diperbarui.']);

        $dosis = [
            'date' => $latest_date,
            'sasaran_nakes' => $res['sasaran_nakes'],
            'sasaran_petugas_publik' => $res['sasaran_petugas_publik'],
            'sasaran_lansia' => $res['sasaran_lansia'],
            'sasaran_masyarakat' => $res['sasaran_masyarakat'],
            'sasaran_remaja' => $res['sasaran_remaja'],
            'sasaran_gotong_royong' => $res['sasaran_gotong_royong'],
            'sasaran_anak' => $res['sasaran_anak'],
            'sasaran_baru' => 0,
        ];
        $dosis_1 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][0]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][0]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][0]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][0]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][0]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][0]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][0]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][0]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][0]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][0]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][0]['vaksin_disabilitas'],
        ];
        $dosis_2 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][1]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][1]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][1]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][1]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][1]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][1]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][1]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][1]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][1]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][1]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][1]['vaksin_disabilitas'],
        ];
        $dosis_3 = [
            ...$dosis,
            'dosis_vaksin' => $res['data'][2]['dosis_vaksin'],
            'vaksin_nakes' => $res['data'][2]['vaksin_nakes'],
            'vaksin_lansia' => $res['data'][2]['vaksin_lansia'],
            'vaksin_petugas_publik' => $res['data'][2]['vaksin_petugas_publik'],
            'vaksin_masyarakat' => $res['data'][2]['vaksin_masyarakat'],
            'vaksin_remaja' => $res['data'][2]['vaksin_remaja'],
            'vaksin_anak' => $res['data'][2]['vaksin_anak'],
            'vaksin_baru' => NULL,
            'vaksin_gotong_royong' => $res['data'][2]['vaksin_gotong_royong'],
            'vaksin_uji_klinis' => $res['data'][2]['vaksin_uji_klinis'],
            'vaksin_ibu_hamil' => $res['data'][2]['vaksin_ibu_hamil'],
            'vaksin_disabilitas' => $res['data'][2]['vaksin_disabilitas'],
        ];

        $insert = Vaksin::upsert([
            $dosis_1,
            $dosis_2,
            $dosis_3
        ], []);

        if (!$insert) return back()->with(['error' => 'Failed to import summary data.']);

        return true;
    }

    public function generate_splp_token()
    {
        $headers = [
            'Authorization' => 'Basic ' . base64_encode(env('SPLP_USER_KEY').':'.env('SPLP_USER_SECRET'))
        ];
        $payload = [
            'grant_type' => 'client_credentials'
        ];

        $response = Http::connectTimeout(20)->retry(3, 200)->withHeaders($headers)->post('https://splp.layanan.go.id/oauth2/token', $payload);
        $data = $response->json();

        if ($response->status() == 200) {
            $this->token = $data['access_token'];

            return true;
        }

        return false;
    }
}
