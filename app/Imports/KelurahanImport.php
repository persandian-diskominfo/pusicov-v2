<?php

namespace App\Imports;

use App\Models\Kelurahan;
use Maatwebsite\Excel\Concerns\ToModel;
// use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;

class KelurahanImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $date = date_create(substr($row[0], 1));
        $day = date_format($date, "d");
        $month = date_format($date, "m");
        $year = date_format($date, "Y");
        $date = "$year-$month-$day";
        
        return new Kelurahan([
            'date' => $date,
            'kecamatan' => $row[1],
            'kelurahan' => ucwords(strtolower($row[2])),
            'total_terkonfirmasi' => $row[3],
            'terkonfirmasi_aktif' => $row[4],
            'terkonfirmasi_sembuh' => $row[5],
            'terkonfirmasi_meninggal' => $row[6],
            'total_suspek' => $row[7],
            'suspek_dalam_pantauan' => $row[8],
            'suspek_discarded' => $row[9],
            'total_kontak_erat' => $row[10],
            'kontak_erat_dalam_pantauan' => $row[11],
            'kontak_erat_discarded' => $row[12],
        ]);
    }

    // public function rules(): array
    // {
    //     return [
    //         '0' => 'required',
    //         '1' => 'required',
    //         '2' => 'required',
    //         '3' => 'required',
    //         '4' => 'required',
    //         '5' => 'required',
    //         '6' => 'required',
    //         '7' => 'required',
    //         '8' => 'required',
    //         '9' => 'required',
    //         '10' => 'required',
    //         '11' => 'required',
    //         '12' => 'required',
    //     ];
    // }
}
