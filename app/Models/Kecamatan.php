<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'trx_kecamatan';

    protected $fillable = [
        'date',
        'kecamatan',
        'total_terkonfirmasi',
        'terkonfirmasi_aktif',
        'terkonfirmasi_sembuh',
        'terkonfirmasi_meninggal',
        'total_suspek',
        'suspek_dalam_pantauan',
        'suspek_discarded',
        'total_kontak_erat',
        'kontak_erat_dalam_pantauan',
        'kontak_erat_discarded'
    ];

    protected $guarded = [];
}
