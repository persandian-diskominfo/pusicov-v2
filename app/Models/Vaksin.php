<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vaksin extends Model
{
    protected $table = 'trx_vaksinasi';

    protected $fillable = [
        'date',
        'dosis_vaksin',
        'sasaran_nakes',
        'sasaran_petugas_publik',
        'sasaran_lansia',
        'sasaran_masyarakat',
        'sasaran_remaja',
        'sasaran_gotong_royong',
        'sasaran_anak',
        'sasaran_baru',
        'vaksin_nakes',
        'vaksin_lansia',
        'vaksin_petugas_publik',
        'vaksin_masyarakat',
        'vaksin_remaja',
        'vaksin_anak',
        'vaksin_baru',
        'vaksin_gotong_royong',
        'vaksin_uji_klinis',
        'vaksin_ibu_hamil',
        'vaksin_disabilitas',
    ];
}
