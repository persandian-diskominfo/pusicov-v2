<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'trx_kelurahan';

    protected $fillable = [
        'date',
        'kecamatan',
        'kelurahan',
        'total_terkonfirmasi',
        'terkonfirmasi_aktif',
        'terkonfirmasi_sembuh',
        'terkonfirmasi_meninggal',
        'total_suspek',
        'suspek_dalam_pantauan',
        'suspek_discarded',
        'total_kontak_erat',
        'kontak_erat_dalam_pantauan',
        'kontak_erat_discarded'
    ];
}
